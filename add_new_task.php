<script type="text/javascipt">
		document.GE
</script>
<?php 
	
	require_once 'init.php';
	//require_once 'classes/Project.php';
	
	//require_once 'classes/Task.php';
	
	$task=new Task();
	
	$tasklists=$task->getTasks();
	
	$project=new Project();
    
	$projectlists=$project->getProjects();

	$user=new User();

	$userlists=$user->getUsers();

	//$project=new Project();

	$taskarray=array();

	if(isset($_POST['submit'])){
		
		var_dump($_POST);

		if(!empty($_POST['taskid'])){
			$taskarray['taskid']=$_POST['taskid'];
		}

		if(!empty($_POST['tasktitle'])){
			$taskarray['tasktitle']=$_POST['tasktitle'];
		}

		if(!empty($_POST['taskdescription'])){
			$taskarray['taskdescription']=$_POST['taskdescription'];
		}

		if(!empty($_POST['projectid'])){
			$taskarray['pid']=$_POST['projectid'];
		}

		if(!empty($_POST['taskhour'])){
			$taskarray['taskhour']=$_POST['taskhour'];
		}

		if(!empty($_POST['parenttask'])){
			$taskarray['parenttaskid']=$_POST['parenttask'];
		}

		if(!empty($_POST['createdby'])){
			$taskarray['createdby']=$_POST['createdby'];
		}
		

		/*$taskarray=array(
			'taskid'=>$taskid,
			'tasktitle'=>$tasktitle,
			'taskdescription'=>$taskdescription,
			'pid'=>$projectid,
			'parenttaskid'=>$parenttask,
			'taskhour'=>$taskhour,
			'createdby'=>$createdby
		);*/

		$insertid=$task->addTask($taskarray);

		if(!empty($insertid)){

			header("Location: project_task_list.php"); 
		}
	}


	


?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Add New Task</h1>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-md-6">
				<div class="box box-primary">
		    		<form action="" method="post" id="add_new_task_form">
		    			<div class="box-body">

		    				<div class="form-group">
						    	<label for="">Task Id:</label>
						        <input type="text" class="form-control required NumbersOnly" placeholder="Task Id" name="taskid" >
						    </div>

						    <div class="form-group">
						    	<label for="">Task Title:</label>
						        <input type="text" class="form-control" placeholder="Task Title" name="tasktitle" required>
						    </div>

						    <div class="form-group">
						    	<label for="">Task Description:</label>
						        <textarea name="taskdescription" class="form-control" id="taskdes" cols="30" rows="10"></textarea>
						    </div>

						    <div class="form-group">
								<label for="">Task hour:</label>
								<input type="text" class="form-control" id="taskhour" placeholder="Task Hour" name="taskhour">
						    </div>

						    <div class="form-group has-feedback">
						    	<label for="">project name:</label>
					      		<select class="form-control" name="projectid" required>
					      			<option value=""></option>
					      			<?php foreach ($projectlists as $projectlist) { ?>
						      	        <option value="<?php echo $projectlist['pid'] ?>"><?php echo $projectlist['title'] ?></option>
						      	    <?php } ?>
					      	    </select>
						    </div>
							
							<div class="form-group has-feedback">
						    	<label for="">Parent Task:</label>
					      		<select class="form-control" name="parenttask">
					      			<option value=""></option>
					      			<?php foreach ($tasklists as $tasklist) { ?>
						      	        <option value="<?php echo $tasklist['taskid'] ?>"><?php echo $tasklist['tasktitle'] ?></option>
						      	    <?php } ?>
					      	    </select>
						    </div>


						    <div class="form-group has-feedback">
						    	<label for="">Created By:</label>
					      		<select class="form-control" name="createdby" required>
					      			<option value=""></option>
					      			<?php foreach ($userlists as $userlist) { ?>
					      	        	<option value="<?php echo $userlist['userid'] ?>"><?php echo $userlist['firstname'] ?> <?php echo $userlist['lastnaame'] ?></option>
					      	        <?php } ?>
					      	    </select>
						    </div>

						    <div class="row">
						        
						        <div class="col-xs-8">

						        </div>
						        
						        <div class="col-xs-4">
						          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
						        </div>
						    </div>

					    </div>
		    		</form>
		    	</div>
			</div>
    	</div>
    </section>
<?php include 'footer.php'; ?>


<script>
  $(function () {
    $("#taskdes").wysihtml5();
  });
</script>