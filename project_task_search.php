<?php require 'init.php';?>

<?php
  
  if(isset($_GET['project_task_search'])){
        $serach=$_GET['project_task_search'];
        $task=new Task();
        $tasklist=$task->getAllTasksearch($serach);   
  }

?>

<?php include 'header.php'; ?>

    <section class="content-header">
      <h1>Task List</h1>
    </section>
    
    <section class="content">

    	<div class="box">
            
            <div class="box-header">
              <h3 class="box-title"></h3>

              <form action="project_task_search.php" method="GET">

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="project_task_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>

              </form>

            </div>
            <!-- /.box-header -->
            <?php if(!empty($tasklist)){ ?>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>TaskTitle</th>
                  <th>taskhour</th>
                  <th>createdby</th>
                  <th>Todo List</th>
                  <th>Taskcompleted</th>
                  <th>Taskdescription</th>
                  <th>Project name</th>
                  <th>Parent Task</th>
                  <th>Action</th>
                </tr>
                <?php foreach ($tasklist as $task) { ?>
                
                <tr>
                  <td><?php echo $task['tasktitle'];?></td>
                  <td><?php echo $task['taskhour'];?></td>
                  <td>
                      <?php 
                          $user=new User();
                          $userlist=$user->getUser($task['createdby']);
                          echo $userlist['firstname'];
                          echo $userlist['lastnaame'];
                        ?>
                    </td>
                  <td>
                      <ul class="tasktodo">
                          
                          <?php
                             $todo=new Todo();
                             $todolists=$todo->gettaktodobytaskid($task['taskid']);
                          ?>

                          <?php  foreach ($todolists as $todolist) { ?>

                            <li> 
                                <?php echo $todolist['todotitle']; ?> 
                                <input type="checkbox" name="todocomplete[]" class="toddocomplete" data-id="<?php echo $todolist['todono'];?>">
                            </li>
                            
                            <li>
                                   
                            </li>
                          
                          <?php } ?>

                          <li><button  type="button" data-id="<?php  echo $task['taskid'];?>" class="btn btn-primary btn-lg AddtodoDialog" data-toggle="modal" data-target="#myModal"> Add New Todo </button></li>

                      </ul>
                  </td>
                  <td></td>
                  <td><?php echo $task['taskdescription'];?></td>
                  <td>  
                        <?php
                             $project=new Project();
                             $projectlist=$project->getProject($task['pid']);
                             echo $projectlist['title'];
                        ?>
                  </td>

                  <td>  <?php
                           echo $task['parenttaskid'];
                          // $tasksinglelist=$task->getTask(1);     
                        ?>
                  </td>

                  <td> <a href="edit_new_task.php?taksid=<?php echo $task['taskid'];?>">Edit</a> | 
                      <a href="delete_new_task.php?taskid=<?php echo $task['taskid'];?>">Delete</a></td>
                </tr>
                <?php } ?>
              </tbody></table>
            </div>
            <?php  } ?>
            <!-- /.box-body -->
          </div>

    </section>


    <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Todo</h4>
      </div>
      <div class="modal-body">
            
          <form action="" method="post" id="add_new_project_task_todo_form">
            <div class="box-body">
                <div class="form-group">
                  <label for="">Todo No:</label>
                    <input type="text" class="form-control required NumbersOnly" placeholder="todono" name="todo no" id="todono"  />
                </div>

                <div class="form-group">
                    <label for="">Todo Title:</label>
                    <input type="text" class="form-control" placeholder="todotitle" name="todotitle" id="todotitle">
                </div>

                <div class="row">
                    
                    <div class="col-xs-8">
                          <input type="hidden" name="modaltaskid" id="modeltaskid">
                    </div>
                    
                    <div class="col-xs-4">
                      
                      <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                    
                    </div>

                </div>
            </div>
          </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php include 'footer.php'; ?>


<script>
  
  jQuery(document).ready(function($) {
  
      $(document).on("click", ".AddtodoDialog", function () {
        var mytodoId = $(this).data('id');
        $("#modeltaskid").val( mytodoId );
      });

      $(".toddocomplete").click( function(){
          
          if( $(this).is(':checked') ){
            var mytodocompleteId = $(this).data('id');

            jQuery.ajax({
              url: 'todocomplete.php',
              type: 'POST',
              dataType: 'html',
              data: {'todono': mytodocompleteId},
              beforeSend: function(){

              },

              success :  function(response){

              }

            });
            
          } 
      });

  });

</script>