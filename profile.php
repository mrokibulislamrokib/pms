<?php include 'header.php'; ?>

<?php
 	require 'init.php';
 	$eprofile=new extendprofile();
	$userprofile=new Userprofile();
	$user=new User();

	$registerarray=array();

	if(isset($_POST['submit'])){

		if(!empty($_POST['firstname'])){
			$registerarray['firstname']=$_POST['firstname'];
		}

		if(!empty($_POST['lastnaame'])){
			$registerarray['lastnaame']=$_POST['lastnaame'];
		}

		if(!empty($_POST['contactno'])){
			$registerarray['contactno']=$_POST['contactno'];
		}

		if(!empty($_POST['street'])){
			$registerarray['street']=$_POST['street'];
		}

		if(!empty($_POST['country'])){
			$registerarray['country']=$_POST['country'];
		}

		if(!empty($_POST['postcode'])){
			$registerarray['postcode']=$_POST['postcode'];
		}

		if(!empty($_POST['city'])){
			$registerarray['city']=$_POST['city'];
		}
		
		$userid=$_SESSION['userid'];

		$user=new User();

		$user->updateUser($registerarray,$userid,$main_id="userid");


		$userprofilearray=array();

		if(!empty($_POST['Secondary_Email'])){
			$userprofilearray['secondaryemail']=$_POST['Secondary_Email'];
		}
		
		$target_dir="uploads/";
		$filename=basename($_FILES['fileToUpload']["name"]);
		$target_file=$target_dir .basename($_FILES['fileToUpload']["name"]);
		$imageFileType=pathinfo($target_file,PATHINFO_EXTENSION);
	
		//if($imageFileType !="jpg" && $imageFileType!="png" && $imageFileType!="jpeg" && $imageFileType!="gif"){

			move_uploaded_file($_FILES['fileToUpload']['tmp_name'],$target_file);

			$userprofilearray['photo']=$filename;

		//}


		$userprofile=new Userprofile();


		$userprofile->updateuserprofile($userprofilearray,$userid,$main_id="userid");

		//exit;
		
		/*$userlist=$user->getUser($_SESSION['userid']);
		$profile_array=array('userid'=>$userlist['userid'],'secondaryemail'=>$secondary_email);
		$userprofile->adduserprofile($profile_array);*/

		$extend=array_combine($_POST['KeY'], $_POST['value']);

		/*foreach ($extend as $key => $value) {
			$ex=array('userid'=>1,'keyitem'=>$key,'valueitem'=>$value);
			$eprofile->addprofile($ex);
		}*/

		//print_r($_POST['KeY']);

		//print_r($_POST['value']);

		/*$i=0;
		foreach ($extend as $key => $value) {
			$ex=array('userid'=>1,'keyitem'=>$key,'valueitem'=>$value);
			$eprofile->addprofile($ex);
		}*/

		for($i=0;$i<count($extend);$i++){
			$ex[$i]=array('userid'=>$_SESSION['userid'],'keyitem'=>$_POST['KeY'][$i],'valueitem'=>$_POST['value'][$i]);
			//print_r($ex[$i]);
			$eprofile=new extendprofile();
			$eprofile->addprofile($ex[$i]);
		}

	}

	$userprofilelist=$userprofile->getprofilebyid($_SESSION['userid']);

?>

<?php 	

		$userid=$_SESSION['userid'];

		$user=new User();

		$userlist=$user->getUser($userid);
?>
		  
    <section class="content-header">
      <h1> User Profile  </h1>
    </section>
    <section class="content">
		<div class="box box-primary">
    		<form action="profile.php" method="post" enctype="multipart/form-data">

				<div class="box-body">

					<div class="row">
						
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							
							<div class="form-group">
						    	<label for="">Firstname:</label>
						        <input type="text" class="form-control" placeholder="Firstname" name="firstname" 
						        value="<?php if(!empty($userlist['firstname'])){ echo $userlist['firstname']; } ?>">
						    </div>

						</div>

						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

							<div class="form-group">
						    	<label for="">Lastname:</label>
						        <input type="text" class="form-control" placeholder="Lastname" name="lastnaame" 
						        value="<?php if(!empty($userlist['firstname'])){ echo $userlist['lastnaame']; } ?>">
						    </div>
							
						</div>

					</div>

					<div class="row">
						
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

							<div class="form-group">
						    	<label for="">Contactno:</label>
						        <input type="text" class="form-control" placeholder="Contactno" name="contactno" 
						        value="<?php if(!empty($userlist['firstname'])){ echo $userlist['contactno']; } ?>">
						    </div>

						</div>

						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
						    	<label for="">Street:</label>
						        <input type="text" class="form-control" placeholder="Street" name="street" 
						        value="<?php if(!empty($userlist['firstname'])){ echo $userlist['street']; } ?>">
						    </div>
						</div>

					</div>
				    
				    
					<div class="row">
						
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
						    	<label for="">Postcode:</label>
						        <input type="text" class="form-control" placeholder="Postcode" name="postcode" 
						        value="<?php if(!empty($userlist['firstname'])){ echo $userlist['postcode']; } ?>">
						    </div>
						</div>

						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

							<div class="form-group">
						    	<label for="">City:</label>
						        <input type="text" class="form-control" placeholder="City" name="city" 
						        value="<?php if(!empty($userlist['firstname'])){ echo $userlist['city']; } ?>">
						    </div>

						</div>

					</div>
				    

				    <div class="row">

				    	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
						    		<label for="">Country:</label>
						      		<select class="form-control" name="country">
						      	        <option>Bangladesh</option>
						      	        <option>India</option>
						      	        <option>Pakistan</option>
						      	        <option>SRILANKA</option>
						      	    </select>
						            <span id="countryerror"></span>
						    </div>
				    	</div>

				    	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

				    	</div>

				    </div>
					
				</div>

    			<div class="box-body">

    				<div class="row">
    					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    						<div class="form-group">
						    	<label for="">Secondary Email:</label>
						        <input type="email" class="form-control" placeholder="Secondary Email" name="Secondary_Email" 
						        value="<?php if(!empty($userprofilelist['secondaryemail'])){ echo $userprofilelist['secondaryemail']; } ?>">
						    </div>
    					</div>
    				</div>

    				<div class="row">
    					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    						<div class="form-group">
							  	<label for="">User Photo</label>
							  	<input type="file" id="fileUpload" name="fileToUpload">
							  	<div id="image-holder"> </div>
							</div>
    					</div>
    				</div>
				</div>
	
				
				<a href="javascript:void(0);" class="add_button btn btn-primary btn-lg">add</a>

	<?php
		
		$exprofiles=$eprofile->getextendprofile($_SESSION['userid']); 

		if(!empty($exprofiles)){ ?>

			<div class="box-body extra"> 

				

				
			
		<?php foreach ($exprofiles as $exprofile) { ?> 
			
			<div class="row rowextra">

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<div class="form-group">
						<label for="">Key :</label>
				        <input type="text" class="form-control" placeholder="Enter Key" name="KeY[]" value="<?php echo $exprofile['keyitem'];?>">
				    </div>
				</div>	

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<div class="form-group">
				    	<label for="">Value:</label>
				        <input type="text" class="form-control" placeholder="Enter Key value" name="value[]" value="<?php echo $exprofile['valueitem'];?>">
				    </div>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

					<!-- <a href="" class="btn btn-danger delete">x</a> -->
				</div>
			</div>
				

	<?php	} ?>

				

			</div>

	<?php } else{ ?>

		<div class="box-body extra">
			<div class="row rowextra">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
		    		<div class="form-group">
						<label for="">Key :</label>
				        <input type="text" class="form-control" placeholder="Enter Key" name="KeY[]">
				    </div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
		    		<label for="">vALUE:</label>
		        	<input type="text" class="form-control" placeholder="Enter Key value" name="value[]">
		        </div>
		        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
		        	<a href="javascript:void(0)" class="btn btn-danger delete">x</a>
		        </div> 
		    </div>
		</div>


	<?php 	} ?>
    			
    			


				<div class="row">
			        
			        <div class="col-xs-10">

			        </div>
			        
			        <div class="col-xs-2">
			          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
			        </div>
			    
			    </div>

			</form>
		</div>

    </section>
<?php include 'footer.php'; ?>


<script>
	jQuery(document).ready(function($) {
	
		$("#fileUpload").on('change', function () {

	        if (typeof (FileReader) != "undefined") {

	            var image_holder = $("#image-holder");
	            image_holder.empty();

	            var reader = new FileReader();
	            reader.onload = function (e) {
	                $("<img />", {
	                    "src": e.target.result,
	                    "class": "thumb-image",
	                    "width":220,
	                    "height":220

	                }).appendTo(image_holder);

	            }
	            image_holder.show();
	            reader.readAsDataURL($(this)[0].files[0]);
	        } else {
	            alert("This browser does not support FileReader.");
	        }
    	});
	});
</script>