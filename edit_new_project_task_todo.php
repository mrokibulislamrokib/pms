<?php 
	
	require 'init.php';

	$todo=new Todo();

	$task=new Task();
	
	$tasklists=$task->getTasks();
	
	$tasktodoarray=array();

	if(isset($_POST['submit'])){

		if(!empty($_POST['todotitle'])){
			$tasktodoarray['todotitle']=$_POST['todotitle'];
		}

		if(!empty($_POST['todono'])){
			$todono=$_POST['todono'];
		}

		if(!empty($_POST['taskname'])){
			$tasktodoarray['taskid']=$_POST['taskname'];
		}

		//$tasktodoarray=array('todotitle' =>$todotitle,'taskid'=>$taskid);

		$todo=new Todo();

		$updateid=$todo->updateTodo($tasktodoarray,$todono,$main_id="todono");

		if(!empty($updateid)){

			header("Location: project_todo_list.php"); 
		}
	}

	if(isset($_GET['todono'])){
		$todono=$_GET['todono'];
		$todo=new Todo();
		$todosinglelist=$todo->gettaskTodo($todono);
	}
?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Edit New Project Task Todo</h1>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-md-6">
				<div class="box box-primary">
		    		<form action="" method="post" id="edit_new_project_task_todo_form">
		    			<div class="box-body">

		    				<div class="form-group">
						    	<label for="">Task Name:</label>
						        <select class="form-control" name="taskname" id="taskname">
						        	<?php foreach ($tasklists as $tasklist) { ?>
					      	        	<option value="<?php echo $tasklist['taskid'] ?>" 
					      	        		<?php if($tasklist['taskid']==$todosinglelist['taskid']){ echo 'selected'; } ?> ><?php echo $tasklist['tasktitle'] ?>
					      	        	</option>

					      	        <?php } ?>
					      	    </select>
						    </div>


						    <div class="form-group">
						    	<label for="">Todo Title:</label>
						        <input type="text" class="form-control" placeholder="todotitle" name="todotitle" id="todotitle" value="<?php echo  
						        $todosinglelist['todotitle'];?>">
						    </div>


						    <div class="row">
						        
						        <div class="col-xs-8">
									<input type="hidden" name="todono"  value="<?php echo $todono;?>">
						        </div>
						        
						        <div class="col-xs-4">
						          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
						        </div>
						    </div>

					    </div>
		    		</form>
		    	</div>
			</div>
    	</div>
    </section>
<?php include 'footer.php'; ?>

