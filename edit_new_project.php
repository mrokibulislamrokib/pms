<?php 
	
	require 'init.php';
	
	$project=new Project();
	
	$projectlist=$project->getProject(1);

	$projectarray=array();
	
	if(isset($_POST['submit'])){

		if(!empty($_POST['title'])){
			$projectarray['title']=$_POST['title'];
		}

		if(!empty($_POST['description'])){
			$projectarray['description']=$_POST['description'];
		}

		if(!empty($_POST['start_date'])){
			$projectarray['startdate']=$_POST['start_date'];
		}

		if(!empty($_POST['duration'])){
			$projectarray['duration']=$_POST['duration'];
		}

		if(!empty($_POST['durationunit'])){
			$projectarray['durationunit']=$_POST['durationunit'];
		}
				
		$pid=$_POST['pid'];
		
		/*$projectarray=array(
			'title'=>$title,
			'description'=>$description,
			'startdate'=>$start_date,
			'duration'=>$duration,
			'durationunit'=>$durationunit
		);*/

		$updateid=$project->updateProject($projectarray ,$pid,$main_id="pid");

		if(!empty($updateid)){
			header("location:project_list.php");
		}

	}


	
	if(isset($_GET['pid'])){
		
		$pid=$_GET['pid'];

		$sproject=$project->getProject($pid);

	} 
		
?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Edit Project</h1>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-md-6">
				<div class="box box-primary">
		    		<form action="edit_new_project.php" method="post" id="edit_new_project_form">
		    			<div class="box-body">
						    
						    <div class="form-group">
						    	<label for="">Title:</label>
						        <input type="text" class="form-control" placeholder="Title" name="title" value="<?php  echo $sproject['title']; ?> <?php  echo $sproject['description']; ?>" id="title">
						    </div>

						    <div class="form-group">
						    	<label for="">Description:</label>
						        <textarea name="description" class="form-control" id="projectdes" cols="30" rows="10" id="description"> <?php  echo $sproject['description']; ?> </textarea>
						    </div>

						    <div class="form-group">
								<label for="">Start Date:</label> 
								<input type="date" class="form-control" id="datepicker" name="start_date" value="<?php  echo $sproject['startdate']; ?>" id="start_date">
						    </div>

						    <div class="form-group">
						    	<label for="">Duration:</label> 
						        <input type="text" class="form-control" placeholder="duration" name="duration" value="<?php  echo $sproject['duration']; ?>" id="duration">
						    </div>


						    <div class="form-group has-feedback">
						    	<label for="">Duration Unit:</label>
					      		<select class="form-control" name="durationunit" id="durationunit">
					      	        <option value="Day" <?php if($sproject['durationunit']=='Day') { 'selected'; } ?> >Day</option>
					      	        <option value="Month" <?php if($sproject['durationunit']=='Month') { 'selected'; } ?>>Month</option>
					      	        <option value="Year" <?php if($sproject['durationunit']=='Year') { 'selected'; } ?>>Year</option>
					      	    </select>
						    </div>



						    <div class="row">
						        
						        <div class="col-xs-8">
									<input type="hidden" name="pid" value="<?php echo $pid?>">
						        </div>
						        
						        <div class="col-xs-4">
						          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
						        </div>
						    </div>

					    </div>
		    		</form>
		    	</div>
			</div>
    	</div>
    </section>
<?php include 'footer.php'; ?>



<script>
  $(function () {
    $("#projectdes").wysihtml5();
  });
</script>