<?php 
  
  require 'init.php';

  $User=new User();

  $registerarray=array();
  
  if(isset($_POST['submit'])){

    if(!empty($_POST['userid'])){
        $userid=mysql_real_escape_string($_POST['userid']);
        $registerarray['userid']=$userid;
    }

    if(!empty($_POST['firstname'])){
        $firstname=mysql_real_escape_string($_POST['firstname']);
        $registerarray['firstname']=$firstname;
    }
    
    if(!empty($_POST['lastname'])){
        $lastname=mysql_real_escape_string($_POST['lastname']);
        $registerarray['lastnaame']=$lastname;
    }
    
    if(!empty($_POST['email'])){
        $email=mysql_real_escape_string($_POST['email']);
        $registerarray['email']=$email;
    }

    if(!empty($_POST['password'])){
        $password=md5(mysql_real_escape_string($_POST['password']));
        $registerarray['passdata']=$password;
    }

    /*if(!empty($_POST['passwordmatch'])){
        $passwordmatch=mysql_real_escape_string($_POST['passwordmatch']);
    }*/


    if(!empty($_POST['contactno'])){
        $contactno=mysql_real_escape_string($_POST['contactno']);
        $registerarray['contactno']=$contactno;
    }

    if(!empty($_POST['street'])){
        $street=mysql_real_escape_string($_POST['street']);
        $registerarray['street']=$street;
    }

    if(!empty($_POST['city'])){
        $city=mysql_real_escape_string($_POST['city']);
        $registerarray['city']=$city;
    }

    if(!empty($_POST['postcode'])){
        $postcode=mysql_real_escape_string($_POST['postcode']);
        $registerarray['postcode']=$postcode;
    }

    if(!empty($_POST['country'])){
        $country=mysql_real_escape_string($_POST['country']);
        $registerarray['country']=$country;
    }


      /*$registerarray=array(
        'firstname'=>$firstname,
        'lastname'=>$lastname,
        'email'=>$email,
        'password'=>$password,
        'contactno'=>$contactno,
        'street'=>$street,
        'city'=>$city,
        'postcode'=>$postcode,
        'country'=>$country
      );*/

     // print_r($registerarray);

      // $User->addUser($registerarray);
      
      $insertid=$User->addUser($registerarray);

      echo $User->getlastuserinsertid();

      echo mysql_insert_id();

      /*if(!empty($insertid)){
        $reuestarray=array('userid'=>$insertid,'confirmation'=>'N');
        $userRequest=new UserRequest();
        $userRequest->addUserRequest($reuestarray);
        header("Location: login.php"); 
      }*/

}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Project Management | Registration</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition register-page">

<div class="register-box">
  <div class="register-logo">
    <a href=""><b>PROJECT</b>MANAGMENT</a>
  </div>
  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form action="register.php" method="post" id="registrationform">

      

      <div class="form-group has-feedback">
        <input type="text" class="form-control required" placeholder="User Id" name="userid"  id="userid" >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
        <span id="userideerror"></span> 
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control required" placeholder="First Name" name="firstname"  id="firstname" >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
        <span id="firstnameerror"></span> 
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control required" placeholder="Last Name" name="lastname" id="lastname">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
        <span id="lastnameerror"></span> 
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control required email" placeholder="Email" name="email" id="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <span id="emailerror"></span> 
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control required" placeholder="Password" name="password" id="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <span id="passworderror"></span> 
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control required" placeholder="Retype password" name="passwordmatch" id="passwordmatch"  equalTo="#password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
        <span id="passwordmatcherror"></span> 
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control NumbersOnly" placeholder="Contact No" name="contactno" id="contactno">
       <!--  <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
       <span id="contactnoerror"></span> 
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Street" name="street" id="street">
       <!--  <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
       <span id="streeterror"></span> 
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="City" name="city" id="city">
       <!--  <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
          <span id="cityerror"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Postcode" name="postcode" id="postcode">
       <!--  <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
       <span id="postcodeerror"></span>
      </div>
      
      <div class="form-group has-feedback">
            <label for="">Country:</label>
      		  <select class="form-control" name="country">
      	        <option>Bangladesh</option>
      	        <option>India</option>
      	        <option>Pakistan</option>
      	        <option>SRILANKA</option>
      	    </select>
            <span id="countryerror"></span>
      </div>

      <div class="row">
        <div class="col-xs-8">
        </div>
        <div class="col-xs-4">
          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat" id="userregister">Register</button>
        </div>
      </div>
    </form>
    <a href="login.html" class="text-center">I already have a membership</a>
  </div>
</div>

<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/iCheck/icheck.min.js"></script>
<script src="dist/js/validation.min.js"></script>
<script src="dist/js/script.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
