<?php 
    require 'init.php';
    $taskScore=new Score();
    $taskScorelistss=$taskScore->getScores();

    if(!empty($taskScorelistss)){
      
      $adjacents = 3;
      $total_pages=count($taskScorelistss);
      $targetpage = "project_task_score_list.php"; 
      $limit = 2;
      $page=0;
      if(isset($_GET['page'])){
        $page=$_GET['page'];
        $start = ($page - 1) * $limit;
        echo $start;
      } else {
          $start = 0;
      }

      $taskScorelists=$taskScore->getproject_task_score_bylimit($start,$limit);

    }
?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Task Score List</h1>
    </section>
    
    <section class="content">

    	<div class="box">
            
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            
            <?php if($taskScorelists) { ?>

            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>Assignment No</th>
                  <th>Score</th>
                  <th>Score date</th>
                  <th>Status</th>
                  <th>comments</th>
                  <th>Action</th>
                </tr>
                <?php foreach ($taskScorelists as $taskScorelist) { ?>
                <tr>
                  <td> <?php echo $taskScorelist['assignno'];?> </td>
                  <td> <?php echo $taskScorelist['score'];?> </td>
                  <td> <?php echo $taskScorelist['scoredate'];?> </td>
                  <td> <?php echo $taskScorelist['status'];?> </td>
                  <td> <?php echo $taskScorelist['comments'];?> </td>
                  <td> <a href="edit_new_project_task_assignment_score.php?assignno=<?php echo $taskScorelist['assignno'];?>">Edit</a>  | <a href="delete_new_project_task_assignment_score.php?assignno=<?php echo $taskScorelist['assignno'];?>"> Delete </a> </td>
                </tr>
                <?php } ?>
              </tbody></table>
            </div>

            <?php } ?>
            <!-- /.box-body -->


                        <?php

  /* Setup page vars for display. */
  if ($page == 0) $page = 1;          //if no page var is given, default to 1.
  $prev = $page - 1;              //previous page is page - 1
  $next = $page + 1;              //next page is page + 1
  $lastpage = ceil($total_pages/$limit);    //lastpage is = total pages / items per page, rounded up.
  $lpm1 = $lastpage - 1;            //last page minus 1
  

  $rokpagination=new rokpagination();
  echo $rokpagination->pagination($adjacents,$total_pages,$targetpage,$page,$prev,$next,$lastpage,$lpm1);
?>

          </div>

    </section>

<?php include 'footer.php'; ?>

