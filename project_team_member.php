<?php 
    require 'init.php';
    $projectteam=new Projectteam();
    $projectteamlistss=$projectteam->getProjectTeamMembers();

    if(!empty($projectteamlistss)){
      
      $adjacents = 3;
      $total_pages=count($projectteamlistss);
      $targetpage = "project_list.php"; 
      $limit = 2;
      $page=0;
      if(isset($_GET['page'])){
        $page=$_GET['page'];
        $start = ($page - 1) * $limit;
        echo $start;
      } else {
          $start = 0;
      }

      $projectteamlists=$projectteam->getprojectteampbylimit($start,$limit);

    }
?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Team Member List</h1>
    </section>
    
    <section class="content">

    	<div class="box">
            
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <?php if(!empty($projectteamlists)) { ?>

            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody>
                <tr>
                  <th>Project Title</th>
                  <th>Projet Member name</th>
                  <th>Projet Member Role</th>
                </tr>
                
                <?php 
                
                    foreach ($projectteamlists as $projectteamlist) { 
                        
                        $projectid=$projectteamlist['pid'];
                        $userid=$projectteamlist['userid'];
                        $roleid=$projectteamlist['roleid'];

                        $project=new Project();
                        $projectlist=$project->getProject($projectid);
                        $user=new User();
                        $userlist=$user->getUser($userid);
                        $userrole=new UserRole();
                        $userolelist=$userrole->getuserRolebyid($roleid);
                ?>


                <tr>
                  <td> <?php echo $projectlist['title'];?> </td>
                  <td> <?php echo $userlist['firstname'];?>  <?php echo $userlist['lastnaame'];?> </td>
                  <td> <?php echo $userolelist['responsibility'];?> </td>
                </tr>
                <?php } ?>
              </tbody></table>
            </div>

            <?php } ?>

            <!-- /.box-body -->



            <?php

  /* Setup page vars for display. */
  if ($page == 0) $page = 1;          //if no page var is given, default to 1.
  $prev = $page - 1;              //previous page is page - 1
  $next = $page + 1;              //next page is page + 1
  $lastpage = ceil($total_pages/$limit);    //lastpage is = total pages / items per page, rounded up.
  $lpm1 = $lastpage - 1;            //last page minus 1
  

  $rokpagination=new rokpagination();
  echo $rokpagination->pagination($adjacents,$total_pages,$targetpage,$page,$prev,$next,$lastpage,$lpm1);
?>

      </div>

    </section>

<?php include 'footer.php'; ?>

