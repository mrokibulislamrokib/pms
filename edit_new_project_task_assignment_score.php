<?php require 'init.php';


	$assignment=new Assignment();

	$assignmentlists=$assignment->getAssignments();

	$score=new Score();

	$scorelists=$score->getScores();

	$taskscore=array();
	
	if(isset($_POST['submit']) && !empty($_POST['assignno']) && !empty($_POST['scoredate'])){
		
		var_dump($_POST);

		if(!empty($_POST['assignno'])){
			$assignno=$_POST['assignno'];
		}

		if(!empty($_POST['score'])){
			$taskscore['score']=$_POST['score'];
		}

		if(!empty($_POST['scoredate'])){
			$taskscore['scoredate']=$_POST['scoredate'];
		}

		if(!empty($_POST['status'])){
			$taskscore['status']=$_POST['status'];
		}
		

		if(!empty($_POST['comments'])){
			$taskscore['comments']=$_POST['comments'];
		}
		
		/*$taskscore=array(
			'score'=>$score,
			'scoredate'=>$taskhour,
			'status'=>$parenttask,
			'comments'=>$createdby,
		);*/

		$score=new Score();

		$updateid=$score->updateScore($taskscore,$assignno,$main_id="assignno");

		if(!empty($updateid)){

				header("Location: project_task_score_list.php"); 
		}
	}

	if(isset($_GET['assignno'])){
		$assignno=$_GET['assignno'];
		$scoresinglelist=$score->getScore($assignno);
	}
?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Add New Project Task assignment Score</h1>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-md-6">
				<div class="box box-primary">
		    		<form action="" method="post" id="edit_new_project_assignment_score_form">
		    			<div class="box-body">

		    				<div class="form-group">
						    	<label for="">assignno:</label>
						        <select class="form-control" name="assignno" id="assignno" required>
						        	<?php foreach ($assignmentlists as $assignmentlist) { ?>
					      	        	<option value="<?php echo $assignmentlist['assignno'];  ?>"> <?php echo $assignmentlist['assignno'];  ?> </option>
					      	        <?php } ?>
					      	    </select>
						    </div>

						    <div class="form-group">
						    	<label for="">score:</label>
						        <input type="text" class="form-control" placeholder="score" name="score" id="score" value="<?php echo $scoresinglelist['score'] ?> ">
						    </div>

						    <div class="form-group">
								<label for="">scoredate:</label>
								<input type="date" class="form-control" id="datepicker" name="scoredate" id="scoredate" placeholder="scoredate" value="<?php echo $scoresinglelist['scoredate'] ?>" required />
						    </div>



						    <div class="form-group">
						    	<label for="">status :</label>
						        <select class="form-control" name="status" id="status">
					      	        <option value=""></option>
					      	        <option value="Y" <?php if($scoresinglelist['status']=='Y') { echo 'selected';}  ?> > Completed </option>
					      	        <option value="N" <?php if($scoresinglelist['status']=='N') { echo 'selected';}  ?> > reject </option>
					      	    </select>
						    </div>

						    <div class="form-group">
						    	<label for="">comments:</label>
						        <textarea name="comments" class="form-control" id="taskscorecomments" cols="30" rows="10" id="comments"> <?php echo $scoresinglelist['comments'] ?> </textarea>
						    </div>


						    <div class="row">
						        
						        <div class="col-xs-8">
									
						        </div>
						        
						        <div class="col-xs-4">
						          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
						        </div>
						    </div>

					    </div>
		    		</form>
		    	</div>
			</div>
    	</div>
    </section>
<?php include 'footer.php'; ?>


<script>
  $(function () {
    $("#taskscorecomments").wysihtml5();
  });
</script>