<?php require 'init.php';

	$task=new Task();

	$tasklists=$task->getTasks();

	$project=new Project();

	$projectlists=$project->getProjects();

	$user=new User();

	$userlists=$user->getUsers();

	

	if(isset($_POST['submit']) && !empty($_POST['taskid']) && !empty($_POST['tasktitle']) && !empty($_POST['projectid']) && !empty($_POST['createdby'])){
		
		var_dump($_POST);
		
		$tasktitle=$_POST['tasktitle'];
		$taskdescription=$_POST['taskdescription'];
		$projectid=$_POST['projectid'];
		$taskhour=$_POST['taskhour'];
		$parenttask=$_POST['parenttask'];
		$createdby=$_POST['createdby'];

		$taskid=$_POST['taskid'];

		$taskarray=array(
			'tasktitle'=>$tasktitle,
			'taskdescription'=>$taskdescription,
			'pid'=>$projectid,
			'parenttaskid'=>$parenttask,
			'taskhour'=>$taskhour,
			'createdby'=>$createdby
		);

		$updateid=$task->updateTask($taskarray,$taskid,$main_id="taskid");

		if(!empty($updateid)){
			header("location:project_task_list.php");
		}
	}


	if(isset($_GET['taksid'])){
		$pid=$_GET['taksid'];
		$task=new Task();
		$tasksinglist=$task->getTask($pid);
		//print_r($tasksinglist);
	}


?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Edit New Task</h1>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-md-6">
				<div class="box box-primary">
		    		<form action="edit_new_task.php" method="post" id="edit_new_task_form">
		    			<div class="box-body">
						    <div class="form-group">
						    	<label for="">Task Title:</label>
						        <input type="text" class="form-control" placeholder="Task Title" name="tasktitle" value="<?php  echo $tasksinglist['tasktitle']; ?>" required>
						    </div>



						    <div class="form-group">
						    	<label for="">Task Description:</label>
						        <textarea name="taskdescription" class="form-control" id="taskdes" cols="30" rows="10" required> <?php echo $tasksinglist['taskdescription']; ?> </textarea>
						    </div>

						    <div class="form-group">
								<label for="">Task hour:</label>
								<input type="text" class="form-control" id="taskhour" placeholder="Task Hour" name="taskhour" value="<?php echo $tasksinglist['taskhour'];?>">
						    </div>

						    <div class="form-group has-feedback">
						    	<label for="">project name:</label>
					      		<select class="form-control" name="projectid" required>
					      			<option value=""></option>
					      			<?php foreach ($projectlists as $projectlist) { ?>
					      	        	<option value="<?php echo $projectlist['pid']; ?>" <?php if($projectlist['pid']== $tasksinglist['pid']){ echo 'selected'; } ?>> 
					      	        		<?php echo $projectlist['title']; ?> 
					      	        	</option>
					      	        <?php } ?>
					      	    </select>
						    </div>
							
							<div class="form-group has-feedback">
						    	<label for="">Parent Task:</label>
					      		<select class="form-control" name="parenttask">
					      			<option value=""></option>
					      			<?php foreach ($tasklists as $tasklist) { ?>
					      	        	<option value="<?php echo $tasklist['taskid'] ?>" <?php if($tasklist['taskid']== $tasksinglist['parenttaskid']){ echo 'selected'; } ?> > 
					      	        			<?php echo $tasklist['tasktitle'] ?> 
					      	        	</option>
					      	        <?php } ?>
					      	    </select>
						    </div>


						    <div class="form-group has-feedback">
						    	<label for="">Created By:</label>
					      		<select class="form-control" name="createdby" required>
					      			<option value=""></option>
					      			<?php foreach ($userlists as $userlist) { ?>
					      	        	<option value="<?php echo $userlist['userid'];?>" 
					      	        			<?php if($userlist['userid']== $tasksinglist['createdby']){ echo 'selected'; } ?>> <?php echo $userlist['firstname'];?>  <?php echo $userlist['lastnaame'];?> </option>
					      	        <?php } ?>
					      	    </select>
						    </div>

						    <div class="row">
						        
						        <div class="col-xs-8">
									<input type="hidden" name="taskid" value="<?php echo $tasksinglelist['taskid']; ?>">
						        </div>
						        
						        <div class="col-xs-4">
						          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
						        </div>
						    </div>

					    </div>
		    		</form>
		    	</div>
			</div>
    	</div>
    </section>
<?php include 'footer.php'; ?>


<script>
  $(function () {
    $("#taskdes").wysihtml5();
  });
</script>