<?php require 'init.php';
    $taskassignment=new Assignment();
    $taskassignmentlists=$taskassignment->getAssignments();

    if(!empty($taskassignmentlists)){
      
      $adjacents = 3;
      $total_pages=count($taskassignmentlists);
      $targetpage = "project_task_assignment_list.php"; 
      $limit = 2;
      $page=0;
      if(isset($_GET['page'])){
        $page=$_GET['page'];
        $start = ($page - 1) * $limit;
        echo $start;
      } else {
          $start = 0;
      }

      $taskassignmentlist=$taskassignment->getproject_task_asignment_bylimit($start,$limit);

    }

?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Assignment List</h1>
    </section>
    
    <section class="content">

    	<div class="box">
            
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->

            <?php if(!empty($taskassignmentlists)){ ?>

            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody>
                <tr>
                  <th>Task Name</th>
                  <th>Assigned by</th>
                  <th>Assigned to</th>
                  <th>Assignment date</th>
                  <th>Deadline</th>
                  <th>Completingdate</th>
                  <th>Action</th>
                </tr>
                <?php foreach ($taskassignmentlists as $taskassignmentlist) { 

                    $user=new User();
                    $userlist=$user->getUser($taskassignmentlist['assignedby']);
                    $userlist=$user->getUser($taskassignmentlist['assignedto']);
                ?>
                <tr>
                  <td>
                  <?php
                    $task=new Task();
                    $tasklist=$task->getTask($taskassignmentlist['taskid']);
                    echo $tasklist['tasktitle'];
                    //print_r($tasklist);
                  ?>

                  </td>
                  <td> <?php echo $userlist['firstname'];?> <?php echo $userlist['lastnaame'];?> </td>
                  <td> <?php echo $userlist['firstname'];?> <?php echo $userlist['lastnaame'];?> </td>
                  <td> <?php echo $taskassignmentlist['assignmentdate'];?> </td>
                  <td> <?php echo $taskassignmentlist['deadline'];?> </td>
                  <td> <?php echo $taskassignmentlist['completingdate'];?> </td>
                  <td> <a href="edit_new_project_task_assignment.php?assignno=<?php echo $taskassignmentlist['assignno'];?>"> Edit </a> | 
                  <a href="delete_new_project_task_assignment.php?assignno=<?php echo $taskassignmentlist['assignno'];?>"> Delete </a> </td>
                </tr>
                <?php } ?>
              </tbody></table>
            </div>

            <?php } ?>

            <!-- /.box-body -->

            <?php

  /* Setup page vars for display. */
  if ($page == 0) $page = 1;          //if no page var is given, default to 1.
  $prev = $page - 1;              //previous page is page - 1
  $next = $page + 1;              //next page is page + 1
  $lastpage = ceil($total_pages/$limit);    //lastpage is = total pages / items per page, rounded up.
  $lpm1 = $lastpage - 1;            //last page minus 1
  

  $rokpagination=new rokpagination();
  echo $rokpagination->pagination($adjacents,$total_pages,$targetpage,$page,$prev,$next,$lastpage,$lpm1);
?>





            <?php

  /* Setup page vars for display. */
  if ($page == 0) $page = 1;          //if no page var is given, default to 1.
  $prev = $page - 1;              //previous page is page - 1
  $next = $page + 1;              //next page is page + 1
  $lastpage = ceil($total_pages/$limit);    //lastpage is = total pages / items per page, rounded up.
  $lpm1 = $lastpage - 1;            //last page minus 1
  

  $rokpagination=new rokpagination();
  echo $rokpagination->pagination($adjacents,$total_pages,$targetpage,$page,$prev,$next,$lastpage,$lpm1);
?>


          </div>

    </section>

<?php include 'footer.php'; ?>

