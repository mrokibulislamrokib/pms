<?php 
    require 'init.php';
    $user=new User();
    $userlists=$user->getUsers();
?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Team Member List List</h1>
    </section>
    
    <section class="content">

    	<div class="box">
            
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <?php if(!empty($userlists)) { ?>

            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody>
                <tr>
                  <th>Firstname</th>
                  <th>Lastname</th>
                  <th>Email</th>
                  <th>Contactno</th>
                  <th>Action</th>
                </tr>
                <?php foreach ($userlists as $userlists) { ?>
                <tr>
                  <td> <?php echo $userlists['firstname'];?> </td>
                  <td> <?php echo $userlists['lastnaame'];?> </td>
                  <td> <?php echo $userlists['email'];?>    </td>
                  <td> <?php echo $userlists['contactno'];?> </td>
                  <td> <a href="<?php echo $userlists['userid'];?>">Edit</a>  | <a href="<?php echo $userlists['userid'];?>">Delete</a> </td>
                </tr>
                <?php } ?>
              </tbody></table>
            </div>

            <?php } ?>

            <!-- /.box-body -->
      </div>

    </section>

<?php include 'footer.php'; ?>

