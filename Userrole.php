<?php
	
	require 'init.php';

	$userrole=new UserRole();

	$userrolelists=$userrole->getuserRole();

	$userrolearray=array();

	if(isset($_POST['submit'])){

		if(!empty($_POST['roleid'])){
			$userrolearray['roleid']=$_POST['roleid'];
		}

		if(!empty($_POST['responsibility'])){
			$userrolearray['responsibility']=$_POST['responsibility'];
		}

		$insertid=$userrole->addUserrole($userrolearray);

		if(!empty($insertid)){
			header("Location: Userrole.php"); 
		}
	}

?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Page Header</h1>
    </section>
    <section class="content">

    	<div class="row">
    		<div class="col-md-6">
				
				<div class="box box-primary">
		    		<form action="" method="post" id="add_new_task_form">
		    			
		    			<div class="box-body">
		    				
		    				<div class="form-group">
						    	<label for="">Role Id:</label>
						        <input type="text" class="form-control required" placeholder="User Role Id" name="roleid" >
							</div>

		    				<div class="form-group">
						    	<label for="">User responsibility:</label>
						        <input type="text" class="form-control required" placeholder="User responsibility" name="responsibility" >
							</div>

							 <div class="row">
						        
						        <div class="col-xs-8">

						        </div>
						        
						        <div class="col-xs-4">
						          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
						        </div>

						    </div>

						</div>

		    		</form> 
				</div>
			</div>

		</div>

		<?php if(!empty($userrolelists)){?>
		
		<div class="row">
    		<div class="col-md-6">	
				<div class="box">
					<div class="box-body table-responsive no-padding">
		              <table class="table table-hover">
		                
		                <tbody>
			                <tr>
			                  <th>roleid</th>
			                  <th>responsibility</th>
			                </tr>
							
							<?php foreach ($userrolelists as $userrolelist) { ?>
			                
				                <tr>
				                	<td> <?php echo $userrolelist['roleid'] ?> </td>
				                	<td> <?php echo $userrolelist['responsibility'] ?> </td>
				                </tr>

				            <?php } ?>
			            
			            </tbody>

			          </table>
					</div>
				</div>
			</div>
		</div>

			<?php } ?>
			


    </section>

<?php include 'footer.php'; ?>

