<?php require 'init.php';
	
	$projectteam=new Projectteam();

	$project=new Project();

	$projectlists=$project->getProjects();

	$user=new User();

	$userlists=$user->getUsers();

	$userrole=new UserRole();

	$userrolelists=$userrole->getuserRole();

	$projectteamarray=array();

	if(isset($_POST['submit'])){

		if(!empty($_POST['projectname'])){
				$projectteamarray['pid']=$_POST['projectname'];
		}

		if(!empty($_POST['teammember'])){
				$projectteamarray['userid']=$_POST['teammember'];
		}

		if(!empty($_POST['teammemberrole'])){
				$projectteamarray['roleid']=$_POST['teammemberrole'];
		}
				
		//$projectteamarray=array('pid'=>$projectname,'userid'=>$teammember,'roleid'=>$teammember);
		
		print_r($projectteamarray);
		
		$insertid=$projectteam->addProjecTteamMember($projectteamarray);

		if(!empty($insertid)){
			header("Location: project_team_member.php"); 
		}
	}


?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Add New Project Team Member</h1>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-md-6">
				<div class="box box-primary">
		    		<form action="" method="post" id="add_new_project_team_form">
		    			<div class="box-body">
						    <div class="form-group">
						    	<label for="">Project Name:</label>
						        <select class="form-control required" name="projectname" id="projectname">
						        	<option value=""></option>
						        	<?php foreach ($projectlists as $projectlist) { ?>
					      	        	<option value="<?php echo $projectlist['pid'];?> "> <?php echo $projectlist['title'];?> </option>
					      	        <?php } ?>
					      	    </select>
						    </div>

						    <div class="form-group">
						    	<label for="">User Name:</label>
						        <select class="form-control required" name="teammember" id="teammember">
						        	<option value=""></option>
						        	<?php foreach ($userlists as $userlist) { ?>
					      	        	<option value="<?php echo $userlist['userid']; ?>"> <?php echo $userlist['firstname']; ?> <?php echo $userlist['lastnaame'];?> </option>
					      	        <?php } ?>
					      	    </select>
						    </div>

						    <div class="form-group">
						    	<label for="">User Role:</label>
						        <select class="form-control required" name="teammemberrole">
						        	<option value=""></option>
						        	<?php foreach ($userrolelists as $userrolelist) { ?>
					      	        	<option value="<?php echo $userrolelist['roleid']; ?>"> <?php echo $userrolelist['responsibility']; ?> </option>
					      	        <?php } ?>
					      	    </select>
						    </div>


						    <div class="row">
						        
						        <div class="col-xs-8">

						        </div>
						        
						        <div class="col-xs-4">
						          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
						        </div>
						    </div>

					    </div>
		    		</form>
		    	</div>
			</div>
    	</div>
    </section>
<?php include 'footer.php'; ?>

