<?php
  
  /*$path = dirname(__FILE__).'/classes';

  echo $path;

  if ($handle = opendir($path)) {

    while (false !== ($file = readdir($handle))) {
        if ((time()-filectime($path.'/'.$file)) < 120) {  // 86400 = 60*60*24
          if (preg_match('/\.php$/i', $file)) {
            unlink($path.'/'.$file);
          }
        }
    }
  }*/
?>

<?php
	require_once 'init.php';
	session_start();
	if(!isset($_SESSION['login_user'])) {
  		header("Location: login.php");
 	} 
?>

<?php

	$userprofile=new Userprofile();
	$profileinfo=$userprofile->getprofilebyid($_SESSION['userid']);
	//print_r($profileinfo);


?>

 <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Project Management</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
  <link rel="stylesheet" href="dist/css/bootstrap3-wysihtml5.min.css">
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<div class="wrapper">
		  <header class="main-header">
		    <a href="home.php" class="logo">
		      <!-- mini logo for sidebar mini 50x50 pixels -->
		      <span class="logo-mini"><b>P</b>M</span>
		      <span class="logo-lg"><b>PROJECT</b>Management</span>
		    </a>

		    <!-- Header Navbar -->
		    <nav class="navbar navbar-static-top" role="navigation">
		      <!-- Sidebar toggle button-->
		      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		        <span class="sr-only">Toggle navigation</span>
		      </a>
		      <!-- Navbar Right Menu -->
		      <div class="navbar-custom-menu">
		        <ul class="nav navbar-nav">
		          <!-- User Account Menu -->
		          <li class="dropdown user user-menu">
		            <!-- Menu Toggle Button -->
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		              <!-- The user image in the navbar-->
		              <img src="uploads/<?php echo $profileinfo['photo']; ?>" class="user-image" alt="User Image">
		              <!-- hidden-xs hides the username on small devices so only the image appears. -->
		              <span class="hidden-xs"> <?php echo $_SESSION['login_user']; ?>  </span>
		            </a>
		            <ul class="dropdown-menu">
		              <!-- The user image in the menu -->
		              <li class="user-header">
		                <img src="uploads/<?php echo $profileinfo['photo']; ?>" class="img-circle" alt="User Image">
						<p><?php  echo $_SESSION['login_user']; ?>  </p>
		              </li>
		              <!-- Menu Body -->
		              <!-- Menu Footer-->
		              <li class="user-footer">
		                <div class="pull-left">
		                  <a href="profile_information.php" class="btn btn-default btn-flat">Profile</a>
		                </div>
		                <div class="pull-right">
		                  <a href="logout.php?logout" class="btn btn-default btn-flat">Sign out</a>
		                </div>
		              </li>
		            </ul>
		          </li>
		          <!-- Control Sidebar Toggle Button -->
		         <!--  <li>
		           <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
		         </li> -->
		        </ul>
		      </div>
		    </nav>
		  </header>
		  
		  <!-- Left side column. contains the logo and sidebar -->
	  
		  <aside class="main-sidebar">

		    <!-- sidebar: style can be found in sidebar.less -->
		    <section class="sidebar">
		      <!-- Sidebar Menu -->
		      <ul class="sidebar-menu">
				<li class=""> <a href="home.php"> <i class="fa fa-dashboard"></i> <span>Dashboard</span> </a> </li>
		        <li class="treeview">
		          <a href="#">
		            <i class="fa fa-dashboard"></i> <span>Project</span>
		            <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
		          </a>
		          <ul class="treeview-menu">
		            <li><a href="add_new_project.php"><i class="fa fa-circle-o"></i> Add New Project</a></li>
		            <li><a href="add_new_project_team_member.php"><i class="fa fa-circle-o"></i> Add New Project Team member</a></li>
		            <li> <a href="project_team_member.php"> <i class="fa fa-circle-o"></i> Project Team Member List </a>

		            <li><a href="project_list.php"><i class="fa fa-circle-o"></i> Project List </a></li>
		          </ul>
		        </li>
		        <li class="treeview">
		          <a href="#">
		            <i class="fa fa-dashboard"></i> <span>Task</span>
		            <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
		          </a>
		          <ul class="treeview-menu">
		            <li><a href="add_new_task.php"><i class="fa fa-circle-o"></i> Add New Task</a></li>
		            <li><a href="add_new_project_task_assignment.php"><i class="fa fa-circle-o"></i> Add New Task assignment</a></li>
		            <li><a href="add_new_project_task_assignment_score.php"><i class="fa fa-circle-o"></i> Add New Task assignment Score</a></li>
		            <li><a href="add_new_project_task_todo.php"><i class="fa fa-circle-o"></i> Add New Task Todo </a></li>
		            <li><a href="project_todo_list.php"><i class="fa fa-circle-o"></i> Task Todo List</a></li>
		            
		            <li> <a href="project_task_assignment_list.php"> <i class="fa fa-circle-o"></i>Task assignment List</a></li>
		            <li> <a href="project_task_score_list.php"> <i class="fa fa-circle-o"></i>Task Score List</a></li>
		            <li><a href="project_task_list.php"><i class="fa fa-circle-o"></i> Task List </a></li>
		          </ul>
		        </li>
		      	<li><a href="profile.php"><i class="fa fa-link"></i> <span>Profile</span></a></li>
		      	<li><a href="Userrole.php"><i class="fa fa-link"></i> <span>Role List</span></a></li>
		      	<li> <?php // if(isset($_SESSION['adminid'])) { ?>
		      			<a href="registration_request.php"><i class="fa fa-link"></i> <span>Registration Request</span></a>
					  <?php // } ?>
		      	</li>
		      	<li>	<?php // if(isset($_SESSION['adminid'])) { ?>
		      				<a href="register_member.php"><i class="fa fa-link"></i> <span>Register Member</span></a>
		      			<?php // } ?>
		      	</li>
		      </ul>
		      <!-- /.sidebar-menu -->
		    </section>
		    <!-- /.sidebar -->
		  </aside>

		  <div class="content-wrapper">

