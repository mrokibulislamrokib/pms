<?php require 'init.php';?>

<?php
  
  if(isset($_GET['project_search'])){
        $serach=$_GET['project_search'];
        $project=new Project();
        $projectlist=$project->getAllProductsearch($serach);   
  }

?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Project List</h1>
    </section>
    
    <section class="content">

      <div class="box">
            
            <div class="box-header">
              <h3 class="box-title"></h3>

                <form action="project_search.php" method="GET">
                  <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                      <input type="text" name="project_search" class="form-control pull-right" placeholder="Search">

                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </div>
                </form>
            </div>
            <!-- /.box-header -->

            <?php if(!empty($projectlist)) { ?>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                  <tbody>
                    <tr>
                      <th>Title</th>
                      <th>Startdate</th>
                      <th>Duration</th>
                      <th>Description</th>
                      <th>Action</th>
                    </tr>
                    
                    <?php foreach ($projectlist as $project) {?>
                      <tr>
                        <td> <?php echo $project['title'];?> </td>
                        <td> <?php echo $project['duration'];?> <?php echo $project['durationunit']; ?> </td>
                        <td> <?php echo $project['startdate'];?> </td>
                        <td> <?php echo $project['description'];?> </td>
                        <td> <a href="">Add Task </a> | <a href="edit_new_project.php?pid=<?php echo $project['pid']; ?> ">Edit</a> | 
                        <a href="delete_new_project.php?pid=<?php echo $project['pid']; ?> ">Delete</a></td>
                      </tr>
                    <?php } ?>
                  </tbody>
              </table>

            </div>

            <?php } ?>
          </div>

    </section>

<?php include 'footer.php'; ?>

