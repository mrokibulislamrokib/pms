<?php 
	
	require 'init.php';

	$todo=new Todo();

	$task=new Task();
	
	$tasklists=$task->getTasks();

	$tasktodoarray=array();
	

	if(isset($_POST['submit'])){

			if(!empty($_POST['todono'])){
				$tasktodoarray['todono']=$_POST['todono'];
			}

			if(!empty($_POST['taskname'])){
				$tasktodoarray['taskid']=$_POST['taskname'];
			}

			if(!empty($_POST['todotitle'])){
				$tasktodoarray['todotitle']=$_POST['todotitle'];
			}

			
			$tasktodoarray['createdate']=date("Y/m/d");

			//$tasktodoarray=array('taskid' =>$taskname,'todotitle' =>$todotitle,'createdate'=>$today ,'todono'=>$todono);

			$todo=new Todo();

			$insertid=$todo->addTodo($tasktodoarray);

			if(!empty($insertid)){

				header("Location:project_todo_list.php"); 
			}
	}


?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Add New Project Task Todo</h1>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-md-6">
				<div class="box box-primary">
		    		<form action="" method="post" id="add_new_project_task_todo_form">
		    			<div class="box-body">

		    				<div class="form-group">
						    	<label for="">Todo No:</label>
						        <input type="text" class="form-control required NumbersOnly" placeholder="todono" name="todo no" id="todono"  />
						    </div>

						     <div class="form-group">
						    	<label for="">Todo Title:</label>
						        <input type="text" class="form-control" placeholder="todotitle" name="todotitle" id="todotitle">
						    </div>

		    				<div class="form-group">
						    	<label for="">Task Name:</label>
						        <select class="form-control required" name="taskname" id="taskname">
						        	<?php foreach ($tasklists as $tasklist) { ?>
					      	        	<option value="<?php echo $tasklist['taskid'] ?>"><?php echo $tasklist['tasktitle'] ?></option>
					      	        <?php } ?>
					      	    </select>
						    </div>

						    <div class="row">
						        
						        <div class="col-xs-8">

						        </div>
						        
						        <div class="col-xs-4">
						          
						          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
						        
						        </div>

						    </div>

					    </div>
		    		</form>
		    	</div>
			</div>
    	</div>
    </section>
<?php include 'footer.php'; ?>

