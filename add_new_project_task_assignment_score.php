<?php 
	
	require 'init.php';

	$score=new Score();

	$assignment=new Assignment();

	$assignmentlists=$assignment->getAssignments();

	$taskscore=array();

	if(isset($_POST['submit'])){
		
		var_dump($_POST);

		if(!empty($_POST['assignno'])){
			$taskscore['assignno']=$_POST['assignno'];
		}

		if(!empty($_POST['score'])){
			$taskscore['score']=$_POST['score'];
		}

		if(!empty($_POST['scoredate'])){
			$taskscore['scoredate']=$_POST['scoredate'];
		}

		if(!empty($_POST['status'])){
			$taskscore['status']=$_POST['status'];
		}
		

		if(!empty($_POST['comments'])){
			$taskscore['comments']=$_POST['comments'];
		}

		print_r($taskscore);

		/*$taskscore=array(
			'assignno'=>$assignno,
			'score'=>$score,
			'scoredate'=>$taskhour,
			'status'=>$parenttask,
			'comments'=>$createdby,
		);*/

		$insertid=$score->addScore($taskscore);

		if(!empty($insertid)){

			header("Location: project_task_score_list.php"); 
		}
	}




?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Add New Project Task assignment Score</h1>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-md-6">
				<div class="box box-primary">
		    		<form action="" method="post" id="add_new_project_task_assignment_score_form">
		    			<div class="box-body">

		    				<div class="form-group">
						    	<label for="">assignno:</label>
						        <select class="form-control required" name="assignno" id="assignno">
						        	<option value=""></option>
						        	<?php foreach ($assignmentlists as $assignmentlist) { ?>
					      	        	<option value="<?php echo $assignmentlist['assignno']; ?>"><?php echo $assignmentlist['assignno']; ?></option>
					      	        <?php } ?>
					      	    </select>
						    </div>

						    <div class="form-group">
						    	<label for="">score:</label>
						        <input type="text" class="form-control" placeholder="score" name="score" id="score">
						    </div>

						    <div class="form-group">
								<label for="">scoredate:</label>
								<input type="date" class="form-control required" id="datepicker" name="scoredate" placeholder="scoredate" id="scoredate">
						    </div>



						    <div class="form-group">
						    	<label for="">status :</label>
						        <select class="form-control" name="status" id="status">
						        	<option value=""></option>
					      	        <option value="Y">Completed</option>
					      	        <option value="N">reject</option>
					      	    </select>
						    </div>

						    <div class="form-group">
						    	<label for="">comments:</label>
						        <textarea name="comments" class="form-control" id="taskscorecomments" cols="30" rows="10" id="comments"></textarea>
						    </div>


						    <div class="row">
						        
						        <div class="col-xs-8">

						        </div>
						        
						        <div class="col-xs-4">
						          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
						        </div>
						    </div>

					    </div>
		    		</form>
		    	</div>
			</div>
    	</div>
    </section>
<?php include 'footer.php'; ?>



<script>
  $(function () {
    $("#taskscorecomments").wysihtml5();
  });
</script>