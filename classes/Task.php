<?php


class Task extends Application{

	private $_table = 'task';


	public function gettaskbylimit($start,$limit){
		 $sql = "SELECT * FROM `{$this->_table}` LIMIT $start, $limit";
		 return $this->db->fetchAll($sql);
	}

	public function getTasks() {
		$sql = " SELECT * FROM `{$this->_table}`";
		return $this->db->fetchAll($sql);
	}


	public function getTask($id) {
		$sql = "SELECT * FROM `{$this->_table}` WHERE `taskid` = '".$this->db->escape($id)."'";
		//echo $sql;
		return $this->db->fetchOne($sql);
	}

	public function getAllTask($srch = null){

		$sql = "SELECT * FROM `{$this->_table}`";
		if (!empty($srch)) {
			$srch = $this->db->escape($srch);
			$sql .= " WHERE `name` LIKE '%{$srch}%' || `id` = '{$srch}'";
		}
		$sql .= " ORDER BY `date` DESC";
		return $this->db->fetchAll($sql);

	}

	public function getAllTasksearch($srch = null){
		$sql = "SELECT * FROM `{$this->_table}`";
		if (!empty($srch)) {
			$srch = $this->db->escape($srch);
			$sql .= " WHERE `tasktitle` LIKE '%{$srch}%'";
		}
		//$sql .= " ORDER BY `date` DESC";
		return $this->db->fetchAll($sql);
	}

	public function addTask($params = null) {

		if (!empty($params)) {
			$this->db->prepareInsert($params);
			$out = $this->db->insert($this->_table);
			$this->_id = $this->db->_id;
			return $out;
		}
		return false;

	}

	public function updateTask($params = null, $id = null,$main_id=null){

		if (!empty($params) && !empty($id)) {
			$this->db->prepareUpdate($params);
			return $this->db->update($this->_table, $id,$main_id);
		}

	}

	public function removeTask($id = null){

		if (!empty($id)) {
			$sql = "DELETE FROM `{$this->_table}`
					WHERE `taskid` = '".$this->db->escape($id)."'";
			return $this->db->query($sql);
		}
		return false;
	}
}