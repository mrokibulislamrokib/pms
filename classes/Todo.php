<?php

class Todo extends Application{

	private $_table = 'todo';

	public function gettasktodobylimit($start,$limit){
		$sql = "SELECT * FROM `{$this->_table}` LIMIT $start, $limit";
		return $this->db->fetchAll($sql);
	}

	public function getCompletedtodobyid($id){
		$sql="SELECT * FROM `{$this->_table}` WHERE `taskid` = '".$this->db->escape($id)."' 
		AND completedate IS NOT NULL";
		return $this->db->fetchAll($sql);
	}

	public function gettaktodobytaskid($id){
		$sql = "SELECT * FROM `{$this->_table}` WHERE `taskid` = '".$this->db->escape($id)."'";
		return $this->db->fetchAll($sql);
	}	

	public function gettasktodos() {
		$sql = "SELECT * FROM `{$this->_table}`";
		return $this->db->fetchAll($sql);
	}


	public function gettaskTodo($id) {
		$sql = "SELECT * FROM `{$this->_table}` WHERE `todono` = '".$this->db->escape($id)."'";
		return $this->db->fetchOne($sql);
	}


	public function addTodo($params = null) {

		if (!empty($params)) {
			$this->db->prepareInsert($params);
			$out = $this->db->insert($this->_table);
			//$this->_id = $this->db->_id;
			return $out;
		}
		return false;
	}

	public function updateTodo($params = null, $id = null,$main_id=null){

		if (!empty($params) && !empty($id)) {
			$this->db->prepareUpdate($params);
			return $this->db->update($this->_table, $id,$main_id);
		}
	}

	public function removeTodo($id = null){
		$sql = "DELETE FROM `{$this->_table}`
					WHERE `todono` = '".$this->db->escape($id)."'";
					echo $sql;
		if (!empty($id)) {
			$sql = "DELETE FROM `{$this->_table}`
					WHERE `todono` = '".$this->db->escape($id)."'";
			
			return $this->db->query($sql);
		}
		return false;
	}
}