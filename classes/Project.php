<?php


class Project extends Application{


	private $_table = 'project';

	public function getprojectpbylimit($start,$limit){
		 $sql = "SELECT * FROM `{$this->_table}` LIMIT $start, $limit";
		 return $this->db->fetchAll($sql);
	}

	public function getProjects() {
		$sql = " SELECT * FROM `{$this->_table}`";
		return $this->db->fetchAll($sql);
	}
	
	public function getProject($id) {
		$sql = "SELECT * FROM `{$this->_table}` WHERE `pid` = '".$this->db->escape($id)."'";
		return $this->db->fetchOne($sql);
	}

	public function getAllProject($srch = null){
		$sql = "SELECT * FROM `{$this->_table}`";
		if (!empty($srch)) {
			$srch = $this->db->escape($srch);
			$sql .= " WHERE `name` LIKE '%{$srch}%' || `id` = '{$srch}'";
		}
		$sql .= " ORDER BY `date` DESC";
		return $this->db->fetchAll($sql);

	}



	public function getAllProductsearch($srch = null) {
		$sql = "SELECT * FROM `{$this->_table}`";
		if (!empty($srch)) {
			$srch = $this->db->escape($srch);
			$sql .= " WHERE `title` LIKE '%{$srch}%'";
		}
		//$sql .= " ORDER BY `date` DESC";
		return $this->db->fetchAll($sql);
	}

	public function addProject($params = null) {
		if (!empty($params)) {
			$this->db->prepareInsert($params);
			$out = $this->db->insert($this->_table);
			//$this->_id = $this->db->_id;
			return $out;
		}
		return false;
	}

	public function updateProject($params = null, $id = null,$main_id=null){
		
		if (!empty($params) && !empty($id)) {
				$this->db->prepareUpdate($params);
				return $this->db->update($this->_table, $id,$main_id);
		}
	}

	public function removeProject($id = null){
		if (!empty($id)) {
			$sql = "DELETE FROM `{$this->_table}`
					WHERE `pid` = '".$this->db->escape($id)."'";
			echo $sql;
			return $this->db->query($sql);
		}
		return false;
	}
	
}