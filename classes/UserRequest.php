<?php


class UserRequest extends Application{

	private $_table = 'registration';

	public function getrequest() {
		$sql = " SELECT * FROM `{$this->_table}` WHERE `confirmation` = 'N'";
		return $this->db->fetchAll($sql);
	}

	public function addUserRequest($params = null) {
		if (!empty($params)) {
			$this->db->prepareInsert($params);
			$out = $this->db->insert($this->_table);
			//$this->_id = $this->db->_id;
			return $out;
		}
		return false;
	}


}