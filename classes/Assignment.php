<?php


class Assignment extends Application{ 

	private $_table = 'taskassignment';


	public function getproject_task_asignment_bylimit($start,$limit){
		 $sql = "SELECT * FROM `{$this->_table}` LIMIT $start, $limit";
		 return $this->db->fetchAll($sql);
	}

	public function getAssignments() {
		$sql = "SELECT * FROM `{$this->_table}`";
		return $this->db->fetchAll($sql);
	}

	public function getAssignment($id) {
		$sql = "SELECT * FROM `{$this->_table}` WHERE `assignno` = '".$this->db->escape($id)."'";
		return $this->db->fetchOne($sql);
	}

	public function addAssignment($params = null) {
		if (!empty($params)) {
			$this->db->prepareInsert($params);
			$out = $this->db->insert($this->_table);
			$this->_id = $this->db->_id;
			return $out;
		}
		return false;
	}

	public function updateAssignment($params = null, $id = null,$main_id=null){
		if (!empty($params) && !empty($id)) {
			$this->db->prepareUpdate($params);
			return $this->db->update($this->_table, $id,$main_id);
		}
	}

	public function removeAssignment($id = null){
		if (!empty($id)) {
			$sql = "DELETE FROM `{$this->_table}`
					WHERE `assignno` = '".$this->db->escape($id)."'";
			return $this->db->query($sql);
		}
		return false;
	}
}