<?php

class UserRole extends Application{

	private $_table = 'userrole';

	public function getuserRole(){
		$sql = " SELECT * FROM `{$this->_table}`";
		return $this->db->fetchAll($sql);
	}

	public function getuserRolebyid($id){
		$sql = " SELECT * FROM `{$this->_table}` WHERE `roleid` = '".$this->db->escape($id)."'";
		return $this->db->fetchOne($sql);
	}

	public function addUserrole($params = null) {
		if (!empty($params)) {
			$this->db->prepareInsert($params);
			$out = $this->db->insert($this->_table);
			$this->_id = $this->db->_id;
			return $out;
		}
		return false;
	}

}