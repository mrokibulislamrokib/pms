<?php


class Projectteam extends Application{


	private $_table = 'projectteam';

	public function getprojectteampbylimit($start,$limit){
		 $sql = "SELECT * FROM `{$this->_table}` LIMIT $start, $limit";
		 return $this->db->fetchAll($sql);
	}

	public function getProjectTeamMembers(){
		$sql = "SELECT * FROM `{$this->_table}`";
		return $this->db->fetchAll($sql);
	}

	public function getProjectTeamMember($id){
		$sql = "SELECT * FROM `{$this->_table}` WHERE `pid` = '".$this->db->escape($id)."'";
		return $this->db->fetchAll($sql);
	}

	

	public function addProjecTteamMember($params = null){
		if (!empty($params)) {
			$this->db->prepareInsert($params);
			$out = $this->db->insert($this->_table);
			//$this->_id = $this->db->_id;
			return $out;
		}
		return false;
	}

	public function updateProjecTteamMember($params = null, $id = null){
		if (!empty($params) && !empty($id)) {
			$this->db->prepareUpdate($params);
			return $this->db->update($this->_table, $id);
		}
	}

	public function removeProjecTteamMember($id = null){
		if (!empty($id)) {
			$sql = "DELETE FROM `{$this->_table}`
					WHERE `id` = '".$this->db->escape($id)."'";
			return $this->db->query($sql);
		}
		return false;
	}

}