<?php

class Userprofile extends Application{

	private $_table = 'userprofile';

	public function getprofilebyid($id) {
		$sql = "SELECT * FROM `{$this->_table}` WHERE `userid` = '".$this->db->escape($id)."'";
		return $this->db->fetchOne($sql);
	}


	public function adduserprofile($params = null) {

		if (!empty($params)) {
			$this->db->prepareInsert($params);
			$out = $this->db->insert($this->_table);
			$this->_id = $this->db->_id;
			return $out;
		}
		return false;

	}


	public function updateuserprofile($params = null, $id = null,$main_id=null){

		if (!empty($params) && !empty($id)) {
				$this->db->prepareUpdate($params);
				return $this->db->update($this->_table, $id,$main_id);
		}
		
	}


	
}