<?php


class User extends Application{

	private $_table = "registration";
	
	public $id;


	public function addUser($params = null, $password = null){

		print_r($params);

		if (!empty($params)) {
			$this->db->prepareInsert($params);
			if ($this->db->insert($this->_table)) {
				return true;
			}
			return false;
		}
		return false;

	}

	public function getlastuserinsertid(){
		return $this->db->lastId();
	}


	public function getByEmail($email = null){

		if (!empty($email)) {
			$sql = "SELECT `id` FROM `{$this->_table}` WHERE `email` = '".$this->db->escape($email)."'
					AND `active` = 1";
			return $this->db->fetchOne($sql);
		}

	}

	public function getUsers(){
		$sql = "SELECT * FROM `{$this->_table}`";
		return $this->db->fetchAll($sql);
	}


	public function getUser($id = null){
		if (!empty($id)) {
			$sql = "SELECT * FROM `{$this->_table}` WHERE `userid` = '".$this->db->escape($id)."'";
			return $this->db->fetchOne($sql);
		}
	}

	public function updateUser($array = null,$id = null,$main_id=null){

		if (!empty($array) && !empty($id)) {
				$this->db->prepareUpdate($array);
				if ($this->db->update($this->_table, $id,$main_id)) {
					return true;
				}
				return false;
		}
	}

	public function getAllUsers($srch = null){
		$sql = "SELECT * FROM `{$this->_table}`";
		if (!empty($srch)) {
			$srch = $this->db->escape($srch);
			$sql .= "WHERE(`first_name` LIKE '%{$srch}%' || `last_name` LIKE '%{$srch}%')";
		}
		$sql .= " ORDER BY `last_name`, `first_name` ASC";
		return $this->db->fetchAll($sql);
	}

	public function checkloginuser(){
	
	}

	/*public function updateuser($params = null, $id = null,$main_id=null){

		if (!empty($params) && !empty($id)) {
				$this->db->prepareUpdate($params);
				return $this->db->update($this->_table, $id,$main_id);
		}
		
	}*/

	public function removeUser($id = null){
		if (!empty($id)) {
				$sql = "DELETE FROM `{$this->_table}` WHERE `userid` = '".$this->db->escape($id)."'";
				return $this->db->query($sql);
		}
	}

}