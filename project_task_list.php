<style>
  .tasktodo{
  list-style-type: none;
}
</style>
<?php 
    require 'init.php';
    $task=new Task();
    $tasklists=$task->getTasks();

    if(!empty($tasklists)){
      
      $adjacents = 3;
      $total_pages=count($tasklists);
      $targetpage = "project_task_list.php"; 
      $limit = 2;
      $page=0;
      if(isset($_GET['page'])){
        $page=$_GET['page'];
        $start = ($page - 1) * $limit;
        echo $start;
      } else {
          $start = 0;
      }

      $tasklist=$task->gettaskbylimit($start,$limit);

    }

?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Task List</h1>
    </section>
    
    <section class="content">

    	<div class="box">
            
            <div class="box-header">
              <h3 class="box-title"></h3>

              <form action="project_task_search.php" method="GET">

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="project_task_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>

              </form>

            </div>
            <!-- /.box-header -->
            <?php if(!empty($tasklist)){ ?>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>TaskTitle</th>
                  <th>taskhour</th>
                  <th>createdby</th>
                  <th>Todo List</th>
                  <th>Taskcompleted</th>
                  <th>Taskdescription</th>
                  <th>Project name</th>
                  <th>Parent Task</th>
                  <th>Action</th>
                </tr>
                <?php foreach ($tasklist as $task) { ?>
                
                <tr>
                  <td><?php echo $task['tasktitle'];?></td>
                  <td><?php echo $task['taskhour'];?></td>
                  <td>
                      <?php 
                          $user=new User();
                          $userlist=$user->getUser($task['createdby']);
                          echo $userlist['firstname'];
                          echo $userlist['lastnaame'];
                        ?>
                    </td>
                  <td>
                      <ul class="tasktodo">
                          
                          <?php
                             $todo=new Todo();
                             $todolists=$todo->gettaktodobytaskid($task['taskid']);
                          ?>

                          <?php  foreach ($todolists as $todolist) { ?>

                            <li>
                                <?php echo $todolist['todotitle']; ?> 
                               
                                <?php if(!empty($todolist['completedate'])) { ?>

                                  <input type="checkbox" name="todocomplete[]" class="toddocomplete" data-id="<?php echo $todolist['todono'];?>" checked>
                                
                              
                                
                                <?php } else { ?>
                                      
                                      <input type="checkbox" name="todocomplete[]" class="toddocomplete" data-id="<?php echo $todolist['todono'];?>">
                                
                                <?php } ?>

                            </li>
                            
                            <li>
                                   
                            </li>
                          
                          <?php } ?>

                          <li><button  type="button" data-id="<?php  echo $task['taskid'];?>" class="btn btn-primary btn-lg AddtodoDialog" data-toggle="modal" data-target="#myModal"> Add New Todo </button></li>

                      </ul>
                  </td>
                  <td>  

                  <?php 
                    $id=$task['taskid'];
                    $todo=new Todo();
                    $todolist=$todo->gettaktodobytaskid($id);
                    $todocompletedlist=$todo->getCompletedtodobyid($id);
                    
                    if(!empty($todocompletedlist) && !empty($todolist)){
                        $completedtodo=count($todocompletedlist);
                        $alltodo=count($todolist);
                        $divided=($completedtodo/$alltodo)*100;
                  ?>
                        <div class="progress">
                          <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $divided; ?>"
                          aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $divided; ?>%">
                            <?php echo $divided; ?>
                          </div>
                        </div>

                    <?php } else{ ?>

                        <div class="progress">
                          <div class="progress-bar" role="progressbar" aria-valuenow="0"
                          aria-valuemin="0" aria-valuemax="100" style="width:0%">
                           0
                          </div>
                        </div>

                     <?php } ?>

                  </td>
                  <td><?php echo $task['taskdescription'];?></td>
                  <td>  
                        <?php
                             $project=new Project();
                             $projectlist=$project->getProject($task['pid']);
                             echo $projectlist['title'];
                        ?>
                  </td>

                  <td>  <?php
                           echo $task['parenttaskid'];
                          // $tasksinglelist=$task->getTask(1);     
                        ?>
                  </td>

                  <td> <a href="edit_new_task.php?taksid=<?php echo $task['taskid'];?>">Edit</a> | 
                      <a href="delete_new_task.php?taskid=<?php echo $task['taskid'];?>">Delete</a></td>
                </tr>
                <?php } ?>
              </tbody></table>
            </div>
            <?php  } ?>


            <?php

  /* Setup page vars for display. */
  if ($page == 0) $page = 1;          //if no page var is given, default to 1.
  $prev = $page - 1;              //previous page is page - 1
  $next = $page + 1;              //next page is page + 1
  $lastpage = ceil($total_pages/$limit);    //lastpage is = total pages / items per page, rounded up.
  $lpm1 = $lastpage - 1;            //last page minus 1
  

  $rokpagination=new rokpagination();
  echo $rokpagination->pagination($adjacents,$total_pages,$targetpage,$page,$prev,$next,$lastpage,$lpm1);
?>


            <!-- /.box-body -->
          </div>

    </section>


    <!-- Button trigger modal -->


<!-- Modal -->

<?php

$tasktodoarray=array();

if(isset($_POST['todomodelsubmit'])){

  if(!empty($_POST['todono'])){

    $tasktodoarray['todono']=$_POST['todono'];
  }

  if(!empty($_POST['todotitle'])){
      
      $tasktodoarray['todotitle']=$_POST['todotitle']; 
  }

  if(!empty($_POST['modaltaskid'])){
      
       $tasktodoarray['taskid']=$_POST['modaltaskid'];
  }

  /*print_r($tasktodoarray);
  exit;*/

  $tasktodoarray['createdate']=date("Y/m/d");

      //$tasktodoarray=array('taskid' =>$taskname,'todotitle' =>$todotitle,'createdate'=>$today ,'todono'=>$todono);

  $todo=new Todo();

  $insertid=$todo->addTodo($tasktodoarray);

  if(!empty($insertid)){

  //header("Location:project_todo_list.php"); ?>

    <script>
         window.location.href='http://localhost/project_management_frontend/project_task_list.php';
    </script>
  

  <?php  }

}

?>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Todo</h4>
      </div>
      <div class="modal-body">
            
          <form action="" method="post" id="add_new_project_task_todo_form">
            <div class="box-body">
                
                <div class="form-group">
                  <label for="">Todo No:</label>
                    <input type="text" class="form-control required NumbersOnly" placeholder="todono" name="todono" id="todono"  />
                </div>

                <div class="form-group">
                    <label for="">Todo Title:</label>
                    <input type="text" class="form-control required" placeholder="todotitle" name="todotitle" id="todotitle">
                </div>

                <div class="row">
                    
                    <div class="col-xs-8">
                          <input type="hidden" name="modaltaskid" id="modeltaskid">
                    </div>
                    
                    <div class="col-xs-4">
                      
                      <button type="submit" name="todomodelsubmit" class="btn btn-primary btn-block btn-flat">Submit</button>
                    
                    </div>

                </div>
            </div>
          </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php include 'footer.php'; ?>


<script type="text/javascript">
  
  jQuery(document).ready(function($) {
  
      $(document).on("click", ".AddtodoDialog", function () {
        var mytodoId = $(this).data('id');
        $("#modeltaskid").val( mytodoId );
      });

      $(".toddocomplete").click( function(){
          
          if( $(this).is(':checked') ){
            
            var mytodocompleteId = $(this).data('id');

            console.log(mytodocompleteId);

            jQuery.ajax({
              url: 'todocomplete.php',
              type: 'POST',
              dataType: 'html',
              data: {'todono': mytodocompleteId},
              beforeSend: function(){

              },

              success :  function(response){

                  location.reload();
              }

            });
            
          } 
      });

  });

</script>