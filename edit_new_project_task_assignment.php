<?php 
	
	require 'init.php';

	$assignment=new Assignment();

	$task=new Task();

	$taskllsts=$task->getTasks();

	$user=new User();

	$userlists=$user->getUsers();

	$assignmentarray=array();


	if(isset($_POST['submit']) && !empty($_POST['assignno']) && !empty($_POST['taskname']) && !empty($_POST['assignby']) && !empty($_POST['Assignmentdate'])){
		
		var_dump($_POST);


		if(!empty($_POST['taskname'])){
			$assignmentarray['taskid']=$_POST['taskname'];
		}


		if(!empty($_POST['assignby'])){
			$assignmentarray['assignedby']=$_POST['assignby'];
		}

		if(!empty($_POST['assignto'])){
			$assignmentarray['assignedto']=$_POST['assignto'];
		}

		if(!empty($_POST['Assignmentdate'])){
			$assignmentarray['assignmentdate']=$_POST['Assignmentdate'];
		}

		if(!empty($_POST['deadline'])){
			$assignmentarray['deadline']=$_POST['deadline'];
		}

		if(!empty($_POST['completingdate'])){
			$assignmentarray['completingdate']=$_POST['completingdate'];
		}

		if(!empty($_POST['assignno'])){
			$assignno=$_POST['assignno'];
		}

		/*$assignmentarray=array(
			'taskid'=>$taskname,
			'assignby'=>$assignby,
			'assignto'=>$assignto,
			'assignmentdate'=>$assignmentdate,
			'deadline'=>$Deadline,
			'completingdate'=>$completingdate
		);*/

		$updateid=$assignment->updateAssignment($assignmentarray,$assignno,$main_id="assignno");

		if(!empty($updateid)){
			header("location:project_task_assignment_list.php");
		}
	}


	if(isset($_GET['assignno'])){
		$assignno=$_GET['assignno'];
		//$assignment=new Assignment();
		$assignlist=$assignment->getAssignment($assignno);
		
		//print_r($assignlist);
	}

?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Edit New Project Task assignment</h1>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-md-6">
				<div class="box box-primary">
		    		<form action="edit_new_project_task_assignment.php" method="post" id="edit_new_project_assignment_form">
		    			<div class="box-body">

		    				<div class="form-group">
						    	<label for="">Task Name:</label>
						        <select class="form-control required" name="taskname" id="taskname">
						        	<option value=""></option>
									<?php foreach ($taskllsts as $taskllst) { ?>
					      	        	<option value="<?php echo $taskllst['taskid']; ?>"
					      	        			<?php if($taskllst['taskid'] == $assignlist['taskid']){ echo 'selected'; } ?>> <?php echo $taskllst['tasktitle']; 
					      	        	 ?>  </option>
					      	        <?php } ?>
					      	    </select>
						    </div>

						    <div class="form-group">
						    	<label for="">Assigned By :</label>
						        <select class="form-control required" name="assignby" id="assignby">
						        	<option value=""></option>
						        	<?php foreach ($userlists as $userlist) { ?>
					      	        	<option value="<?php echo $userlist['userid']; ?>"
					      	        		<?php  if($userlist['userid'] == $assignlist['assignedby']){ echo 'selected'; } ?>
					      	        	> 
					      	        			<?php echo $userlist['firstname']; ?> <?php echo $userlist['lastnaame']; ?> </option>
					      	        <?php } ?>
					      	    </select>
						    </div>

						    <div class="form-group">
						    	<label for="">Assigned TO :</label>
						        <select class="form-control required" name="assignto" id="assignto">
						        	<option value=""></option>
					      	        <?php foreach ($userlists as $userlist) { ?>
					      	        	<option value="<?php echo $userlist['userid']; ?>" <?php  if($userlist['userid'] == $assignlist['assignedby']){ echo 'selected'; } ?> > 
					      	        		<?php echo $userlist['firstname']; ?> <?php echo $userlist['lastnaame']; ?> 
					      	        	</option>
					      	        <?php } ?>
					      	    </select>
						    </div>



						    <div class="form-group">
						    	<label for="">Assignmentdate:</label>
						        <input type="date" class="form-control" placeholder="Assignment Date" name="Assignmentdate" value="<?php echo $assignlist['assignmentdate'];?>" id="Assignmentdate" required/>
						    </div>

						    

						    <div class="form-group">
								<label for="">deadline:</label>
								<input type="date" class="form-control" id="datepicker" name="deadline" placeholder="Deadline"
								value="<?php echo $assignlist['deadline'];?>" id="deadline">
						    </div>

						    <div class="form-group">
						    	<label for="">completingdate:</label>
						        <input type="date" class="form-control" placeholder="completingdate" name="completingdate"
						        value="<?php echo $assignlist['completingdate'];?>" id="completingdate">
						    </div>


						    <div class="row">
						        
						        <div class="col-xs-8">
									<input type="hidden" name="assignno" value="<?php echo $assignno; ?>">
						        </div>
						        
						        <div class="col-xs-4">
						          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
						        </div>
						    </div>

					    </div>
		    		</form>
		    	</div>
			</div>
    	</div>
    </section>
<?php include 'footer.php'; ?>

