<?php 
	
	require 'init.php';

	$assignment=new Assignment();

	$task=new Task();
	
	$tasklists=$task->getTasks();
	
	$user=new User();

	$userlists=$user->getUsers();

	$assignmentarray=array();
		

	if(isset($_POST['submit']) && !empty($_POST['assignno']) && !empty($_POST['taskname']) && !empty($_POST['assignby']) && !empty($_POST['Assignmentdate']) ){
		
		var_dump($_POST);

		if(!empty($_POST['assignno'])){
			$assignmentarray['assignno']=$_POST['assignno'];
		}

		if(!empty($_POST['taskname'])){
			$assignmentarray['taskid']=$_POST['taskname'];
		}


		if(!empty($_POST['assignby'])){
			$assignmentarray['assignedby']=$_POST['assignby'];
		}

		if(!empty($_POST['assignto'])){
			$assignmentarray['assignedto']=$_POST['assignto'];
		}

		if(!empty($_POST['Assignmentdate'])){
			$assignmentarray['assignmentdate']=$_POST['Assignmentdate'];
		}

		if(!empty($_POST['deadline'])){
			$assignmentarray['deadline']=$_POST['deadline'];
		}

		if(!empty($_POST['completingdate'])){
			$assignmentarray['completingdate']=$_POST['completingdate'];
		}

		/*$assignmentarray=array(
			'assignno'=>$assignno,
			'taskid'=>$taskname,
			'assignedby'=>$assignby,
			'assignedto'=>$assignto,
			'assignmentdate'=>$assignmentdate,
			'deadline'=>$Deadline,
			'completingdate'=>$completingdate
		);*/

		$insertid=$assignment->addAssignment($assignmentarray);

		if(!empty($insertid)){

			header("Location: project_task_assignment_list.php"); 
		}
	}


?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Add New Project Task assignment</h1>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-md-6">
				<div class="box box-primary">
		    		<form action="" method="post" id="add_new_project_task_assignment_form">
		    			<div class="box-body">

		    				

		    				<div class="form-group">
						    	<label for="">assignno:</label>
						        <input type="text" class="form-control required NumbersOnly" id="assignno" name="assignno" placeholder="assignno">
						    </div>

		    				<div class="form-group">
						    	<label for="">Task Name:</label>
						        <select class="form-control required" name="taskname" id="taskname">
						        	<option value=""></option>
						        	<?php foreach ($tasklists as $tasklist) { ?>
					      	        	<option value="<?php echo $tasklist['taskid'] ?>"><?php echo $tasklist['tasktitle'] ?></option>
					      	        <?php } ?>
					      	    </select>
						    </div>

						    <div class="form-group">
						    	<label for="">Assigned By :</label>
						        <select class="form-control required" name="assignby" id="assignby">
						        	<option value=""></option>
					      	        <?php foreach ($userlists as $userlist) { ?>
					      	        	<option value="<?php echo $userlist['userid'] ?>"><?php echo $userlist['firstname'] ?> <?php echo $userlist['lastnaame'] ?></option>
					      	        <?php } ?>
					      	    </select>
						    </div>

						    <div class="form-group">
						    	<label for="">Assigned TO :</label>
						        <select class="form-control required" name="assignto" id="assignto">
						        	<option value=""></option>
					      	        <?php foreach ($userlists as $userlist) { ?>
					      	        	<option value="<?php echo $userlist['userid'] ?>"><?php echo $userlist['firstname'] ?> <?php echo $userlist['lastnaame'] ?></option>
					      	        <?php } ?>
					      	    </select>
						    </div>



						    <div class="form-group">
						    	<label for="">Assignmentdate:</label>
						        <input type="date" class="form-control required" placeholder="Assignment Date" name="Assignmentdate" id="Assignmentdate"/>
						    </div>

						    

						    <div class="form-group">
								<label for="">deadline:</label>
								<input type="date" class="form-control" id="datepicker" name="deadline" placeholder="Deadline" id="deadline"/>
						    </div>

						    <div class="form-group">
						    	<label for="">completingdate:</label>
						        <input type="date" class="form-control" placeholder="completingdate" name="completingdate" id="completingdate"/>
						    </div>


						    <div class="row">
						        
						        <div class="col-xs-8">

						        </div>
						        
						        <div class="col-xs-4">
						          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
						        </div>
						    </div>

					    </div>
		    		</form>
		    	</div>
			</div>
    	</div>
    </section>
<?php include 'footer.php'; ?>

