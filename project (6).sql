-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2017 at 11:14 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `userid` varchar(30) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastnaame` varchar(50) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `contactno` char(15) NOT NULL,
  `street` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `postcode` varchar(10) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `passdata` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`userid`, `firstname`, `lastnaame`, `email`, `contactno`, `street`, `city`, `postcode`, `country`, `passdata`) VALUES
('1', 'sebastan', 'gunzalbaz', 'rrcu@gmail.com', '23567', 'road2', 'ctg', '4200', 'bn', '5a105cc6b7560b497857123aebdf17fe');

-- --------------------------------------------------------

--
-- Table structure for table `extendedprofile`
--

CREATE TABLE `extendedprofile` (
  `userid` varchar(30) NOT NULL,
  `keyitem` varchar(255) NOT NULL,
  `valueitem` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `extendedprofile`
--

INSERT INTO `extendedprofile` (`userid`, `keyitem`, `valueitem`) VALUES
('1', 'ssc', 'sanit janits school'),
('1', 'hsc', 'durban college'),
('1', 'ssc', 'sanit janits school'),
('1', 'hsc', 'durban college');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `pid` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `duration` int(11) DEFAULT '0',
  `durationunit` char(5) DEFAULT 'DAY'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`pid`, `title`, `description`, `startdate`, `duration`, `durationunit`) VALUES
(1, 'zend theme development ', '  dfda', '2017-03-18', 20, 'Day'),
(7, 'backbone js', 'afd', '2017-03-04', 7, 'Day'),
(9, 'cakephp application development', 'fadslkfdsal', '2017-03-11', 30, 'Day'),
(24, 'woocommerce', 'afdsaf', '2017-03-31', 30, 'Day');

-- --------------------------------------------------------

--
-- Table structure for table `projectteam`
--

CREATE TABLE `projectteam` (
  `pid` int(11) NOT NULL,
  `userid` varchar(30) NOT NULL,
  `roleid` char(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projectteam`
--

INSERT INTO `projectteam` (`pid`, `userid`, `roleid`) VALUES
(1, '1', '1'),
(24, '1', '1'),
(24, '1', '1'),
(9, '1', '1'),
(7, '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `userid` varchar(30) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastnaame` varchar(50) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `contactno` char(15) NOT NULL,
  `street` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `postcode` varchar(10) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `passdata` varchar(64) NOT NULL,
  `confirmation` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`userid`, `firstname`, `lastnaame`, `email`, `contactno`, `street`, `city`, `postcode`, `country`, `passdata`, `confirmation`) VALUES
('1', 'sebastan rahul', 'guzalbaz', 'rrcu@gmail.com', '2356789', 'road2', 'ctg', '4200', 'Bangladesh', '5a105cc6b7560b497857123aebdf17fe', 'Y'),
('2', 'james', 'bond', 'kkcu@gmail.com', '2356789', 'road2', 'ctg', '4200', 'Bangladesh', '5a105cc6b7560b497857123aebdf17fe', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `taskid` int(11) NOT NULL,
  `tasktitle` varchar(100) NOT NULL,
  `taskdescription` varchar(255) DEFAULT NULL,
  `pid` int(11) NOT NULL,
  `parenttaskid` int(11) DEFAULT NULL,
  `taskhour` int(11) DEFAULT '0',
  `createdby` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`taskid`, `tasktitle`, `taskdescription`, `pid`, `parenttaskid`, `taskhour`, `createdby`) VALUES
(0, 'normalization', ' fasdfdsaf ', 9, 0, 20, '1'),
(1, 'normalization', 'fasdfdsaf', 1, 1, 20, '1'),
(23, 'woocommerce normalization', 'woocommerce db make normalization', 24, 1, 13, '1'),
(34, 'class diagram', 'fafkdsaffadsf', 9, 1, 5, '1'),
(35, 'sequence diagram', 'fdsafds', 24, 1, 3, '1');

-- --------------------------------------------------------

--
-- Table structure for table `taskassignment`
--

CREATE TABLE `taskassignment` (
  `assignno` int(11) NOT NULL,
  `taskid` int(11) NOT NULL,
  `assignedby` varchar(30) NOT NULL,
  `assignedto` varchar(30) NOT NULL,
  `assignmentdate` date NOT NULL,
  `deadline` datetime DEFAULT NULL,
  `completingdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taskassignment`
--

INSERT INTO `taskassignment` (`assignno`, `taskid`, `assignedby`, `assignedto`, `assignmentdate`, `deadline`, `completingdate`) VALUES
(1, 1, '1', '1', '2017-03-08', '2017-03-16 00:00:00', '2017-03-24 00:00:00'),
(5, 34, '1', '1', '2017-03-21', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `taskscore`
--

CREATE TABLE `taskscore` (
  `assignno` int(11) NOT NULL,
  `score` float DEFAULT '0',
  `scoredate` datetime NOT NULL,
  `status` char(1) DEFAULT 'N',
  `comments` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taskscore`
--

INSERT INTO `taskscore` (`assignno`, `score`, `scoredate`, `status`, `comments`) VALUES
(1, 40, '2017-03-20 00:00:00', 'N', '  fdsafds  ');

-- --------------------------------------------------------

--
-- Table structure for table `todo`
--

CREATE TABLE `todo` (
  `todono` int(11) NOT NULL,
  `todotitle` varchar(100) DEFAULT NULL,
  `createdate` datetime NOT NULL,
  `completedate` datetime DEFAULT NULL,
  `taskid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `todo`
--

INSERT INTO `todo` (`todono`, `todotitle`, `createdate`, `completedate`, `taskid`) VALUES
(2, 'dsfas', '2017-03-25 00:00:00', '2017-03-25 00:00:00', 23),
(3, '1st normal form', '2017-03-18 00:00:00', '2017-03-25 00:00:00', 1),
(5, 'angular based ajax application development', '2017-03-25 00:00:00', '2017-03-25 00:00:00', 0),
(6, '2nd normalform', '2017-03-25 00:00:00', '2017-03-25 00:00:00', 0),
(7, '3rd normalform', '2017-03-25 00:00:00', '2017-03-25 00:00:00', 0),
(8, '4th normalform', '2017-03-25 00:00:00', '2017-03-25 00:00:00', 1),
(9, '5th normalform', '2017-03-25 00:00:00', '2017-03-25 00:00:00', 1),
(11, 'java', '2017-03-25 00:00:00', '2017-03-25 00:00:00', 23),
(12, 'c#', '2017-03-25 00:00:00', '2017-03-25 00:00:00', 23),
(13, 'sequence diagram', '2017-03-29 00:00:00', '2017-03-29 00:00:00', 1),
(16, 'sdafdsaf', '2017-03-29 00:00:00', '2017-03-29 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `userprofile`
--

CREATE TABLE `userprofile` (
  `userid` varchar(30) NOT NULL,
  `secondaryemail` varchar(100) NOT NULL,
  `photo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userprofile`
--

INSERT INTO `userprofile` (`userid`, `secondaryemail`, `photo`) VALUES
('1', 'k@gmail.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `userrole`
--

CREATE TABLE `userrole` (
  `roleid` char(7) NOT NULL,
  `responsibility` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userrole`
--

INSERT INTO `userrole` (`roleid`, `responsibility`) VALUES
('1', 'db\r\n'),
('12', 'team leader');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `extendedprofile`
--
ALTER TABLE `extendedprofile`
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `projectteam`
--
ALTER TABLE `projectteam`
  ADD KEY `pid` (`pid`),
  ADD KEY `userid` (`userid`),
  ADD KEY `roleid` (`roleid`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`taskid`),
  ADD KEY `pid` (`pid`),
  ADD KEY `createdby` (`createdby`),
  ADD KEY `parenttaskid` (`parenttaskid`);

--
-- Indexes for table `taskassignment`
--
ALTER TABLE `taskassignment`
  ADD PRIMARY KEY (`assignno`),
  ADD KEY `taskid` (`taskid`),
  ADD KEY `assignedby` (`assignedby`),
  ADD KEY `assignedto` (`assignedto`);

--
-- Indexes for table `taskscore`
--
ALTER TABLE `taskscore`
  ADD PRIMARY KEY (`assignno`,`scoredate`);

--
-- Indexes for table `todo`
--
ALTER TABLE `todo`
  ADD PRIMARY KEY (`todono`),
  ADD KEY `taskid` (`taskid`);

--
-- Indexes for table `userprofile`
--
ALTER TABLE `userprofile`
  ADD UNIQUE KEY `userid` (`userid`);

--
-- Indexes for table `userrole`
--
ALTER TABLE `userrole`
  ADD PRIMARY KEY (`roleid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `todo`
--
ALTER TABLE `todo`
  MODIFY `todono` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `extendedprofile`
--
ALTER TABLE `extendedprofile`
  ADD CONSTRAINT `extendedprofile_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `registration` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `projectteam`
--
ALTER TABLE `projectteam`
  ADD CONSTRAINT `projectteam_ibfk_1` FOREIGN KEY (`pid`) REFERENCES `project` (`pid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `projectteam_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `registration` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `projectteam_ibfk_3` FOREIGN KEY (`roleid`) REFERENCES `userrole` (`roleid`) ON UPDATE CASCADE;

--
-- Constraints for table `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `task_ibfk_1` FOREIGN KEY (`pid`) REFERENCES `project` (`pid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `task_ibfk_2` FOREIGN KEY (`createdby`) REFERENCES `registration` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `task_ibfk_3` FOREIGN KEY (`parenttaskid`) REFERENCES `task` (`taskid`) ON UPDATE CASCADE;

--
-- Constraints for table `taskassignment`
--
ALTER TABLE `taskassignment`
  ADD CONSTRAINT `taskassignment_ibfk_1` FOREIGN KEY (`taskid`) REFERENCES `task` (`taskid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `taskassignment_ibfk_2` FOREIGN KEY (`assignedby`) REFERENCES `registration` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `taskassignment_ibfk_3` FOREIGN KEY (`assignedto`) REFERENCES `registration` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `taskscore`
--
ALTER TABLE `taskscore`
  ADD CONSTRAINT `taskscore_ibfk_1` FOREIGN KEY (`assignno`) REFERENCES `taskassignment` (`assignno`) ON UPDATE CASCADE;

--
-- Constraints for table `todo`
--
ALTER TABLE `todo`
  ADD CONSTRAINT `todo_ibfk_1` FOREIGN KEY (`taskid`) REFERENCES `task` (`taskid`) ON UPDATE CASCADE;

--
-- Constraints for table `userprofile`
--
ALTER TABLE `userprofile`
  ADD CONSTRAINT `userprofile_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `registration` (`userid`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
