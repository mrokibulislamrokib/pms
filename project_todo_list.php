<?php 
    require 'init.php';
    $todo=new Todo();
    $todolistss=$todo->gettasktodos();

    if(!empty($todolistss)){
      
      $adjacents = 3;
      $total_pages=count($todolistss);
      $targetpage = "project_todo_list.php"; 
      $limit = 2;
      $page=0;
      if(isset($_GET['page'])){
        $page=$_GET['page'];
        $start = ($page - 1) * $limit;
        echo $start;
      } else {
          $start = 0;
      }

      $todolists=$todo->gettasktodobylimit($start,$limit);

    }
?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Todo List</h1>
    </section>
    
    <section class="content">

    	<div class="box">
            
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->

            <?php if(!empty($todolists)) { ?>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody>
                  <tr>
                    <th>Title</th>
                    <th>createdate</th>
                    <th>completedate</th>
                    <th>tasktitle</th>
                    <th>Action</th>
                  </tr>
                  
                  <?php foreach ($todolists as $todolist) {
                        $task=new Task();
                        $tasklist=$task->getTask($todolist['taskid']);

                    ?>
                    <tr>
                      <td> <?php echo $todolist['todotitle'];?> </td>
                      <td> <?php echo $todolist['createdate'];?> </td>
                      <td> <?php echo $todolist['completedate'];?> </td>
                      <td> <?php echo $tasklist['tasktitle'];?> </td>
                      <td> <a href="">Add Task </a> | 
                           <a href="edit_new_project_task_todo.php?todono=<?php echo $todolist['todono'];?>">Edit</a> | 
                           <a href="delete_new_project_task_todo.php?todono=<?php echo $todolist['todono'];?> ">Delete</a></td>
                    </tr>
                  <?php } ?>
              </tbody></table>
            </div>

            <?php } ?>


            <?php

  /* Setup page vars for display. */
  if ($page == 0) $page = 1;          //if no page var is given, default to 1.
  $prev = $page - 1;              //previous page is page - 1
  $next = $page + 1;              //next page is page + 1
  $lastpage = ceil($total_pages/$limit);    //lastpage is = total pages / items per page, rounded up.
  $lpm1 = $lastpage - 1;            //last page minus 1
  

  $rokpagination=new rokpagination();
  echo $rokpagination->pagination($adjacents,$total_pages,$targetpage,$page,$prev,$next,$lastpage,$lpm1);
?>

            <!-- /.box-body -->
          </div>

    </section>

<?php include 'footer.php'; ?>

