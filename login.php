<?php
  
  /*$path = dirname(__FILE__).'/classes';

  if ($handle = opendir($path)) {

    while (false !== ($file = readdir($handle))) {

      if ((time()-filectime($path.'/'.$file)) < 120) {
        if (preg_match('/\.php$/i', $file)) {
             
            // unlink($path.'/'.$file);
        }

      }
    
    }

  }*/

  /*if ($handle = opendir($path)) {

    while (false !== ($file = readdir($handle))) {
        if ((time()-filectime($path.'/'.$file)) < 120) {  // 86400 = 60*60*24
          if (preg_match('/\.php$/i', $file)) {
            unlink($path.'/'.$file);
          }
        }
    }
  }*/
?>

<?php 
session_start();
require 'init.php';
	
  if(isset($_POST['submit'])){    
      if(!empty($_POST['email'])){
          $email=mysql_real_escape_string($_POST['email']);
      }

      if(!empty($_POST['password'])){
          $password=md5(mysql_real_escape_string($_POST['password']));
      }

      $loginclass=new Login();
    
      $loginclass->loginuser($email,$password);
	}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Project Management | Log in</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">

<div class="login-box">
  <div class="login-logo">
    <a href=""><b>PROJECT</b>MANAGMENT</a>
  </div>

  <div class="login-box-body">
    <p class="login-box-msg"></p>
    <form  action="login.php" method="post" id="login-form">
      <div class="form-group has-feedback">
        <input type="email" class="form-control required" placeholder="Email" name="email" id="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <span id="emailerror"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control required" placeholder="Password" name="password" id="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <span id="passworderror"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">

        </div>
        <div class="col-xs-4">
          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat" >Sign In</button>
        </div>
      </div>
    </form>
<!-- 
    <a href="#">I forgot my password</a><br> -->
    <a href="register.php" class="text-center">Register a new membership</a>
  </div>
</div>

<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/iCheck/icheck.min.js"></script>
<script src="dist/js/validation.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
