<script>

</script>

<?php

 	require 'init.php';
	
	$project=new Project();

	$projectarray=array();

	if(isset($_POST['submit']) && !empty($_POST['projectid']) && !empty($_POST['title'])){

		if(!empty($_POST['projectid'])){
			$projectarray['pid']=$_POST['projectid'];
		}

		if(!empty($_POST['title'])){
			$projectarray['title']=$_POST['title'];
		}

		if(!empty($_POST['description'])){
			$projectarray['description']=$_POST['description'];
		}

		if(!empty($_POST['start_date'])){
			$projectarray['startdate']=$_POST['start_date'];
		}

		if(!empty($_POST['duration'])){
			$projectarray['duration']=$_POST['duration'];
		}

		if(!empty($_POST['durationunit'])){
			$projectarray['durationunit']=$_POST['durationunit'];
		}


		/*$projectarray=array(
			'pid'=>$projectid,
			'title'=>$title,
			'description'=>$description,
			'startdate'=>$start_date,
			'duration'=>$duration,
			'durationunit'=>$durationunit
		);*/

		$insertid=$project->addProject($projectarray);


		if(!empty($insertid)){
			header("Location: project_list.php"); 
		}
	}


?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Add New Project</h1>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-md-6">
				<div class="box box-primary">
		    		<form action="add_new_project.php" method="post" id="add_new_project_form">
		    			<div class="box-body">


		    				<div class="form-group">
						    	<label for="">Project Id:</label>
						        <input type="text" class="form-control required NumbersOnly" placeholder="Project Id" name="projectid" id="projectid" />
						    </div>

						    
						    <div class="form-group">
						    	<label for="">Title:</label>
						        <input type="text" class="form-control required" placeholder="Title" name="title" id="title" />
						    </div>

						    <div class="form-group">
						    	<label for="">Description:</label>
						        <textarea name="description" class="form-control" id="projectdes" cols="30" rows="10" id="description"></textarea>
						    </div>

						    <div class="form-group">
								<label for="">Start Date:</label>
								<input type="date" class="form-control" id="datepicker" name="start_date" class="start_date"/>
						    </div>

						    <div class="form-group">
						    	<label for="">Duration:</label>
						        <input type="text" class="form-control" placeholder="duration" name="duration" id="duration"/>
						    </div>


						    <div class="form-group has-feedback">
						    	<label for="">Duration Unit:</label>
					      		<select class="form-control" name="durationunit" id="durationunit">
					      			<option value=""></option>
					      	        <option value="Day">Day</option>
					      	        <option value="Month">Month</option>
					      	        <option value="Year">Year</option>
					      	    </select>
						    </div>

						    <div class="row">
						        
						        <div class="col-xs-8">

						        </div>
						        
						        <div class="col-xs-4">
						          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
						        </div>
						    </div>

					    </div>
		    		</form>
		    	</div>
			</div>
    	</div>
    </section>
<?php include 'footer.php'; ?>


<script>
  $(function () {
    $("#projectdes").wysihtml5();
  });
</script>