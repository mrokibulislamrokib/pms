<?php 
  require 'init.php';
  $userrequest=new UserRequest();
  $userrequestlists=$userrequest->getrequest();
?>

<?php include 'header.php'; ?>
		  
    <section class="content-header">
      <h1>Registration Request List</h1>
    </section>
    <section class="content">
		
    <?php if(!empty($userrequestlists)) { ?>

		<div class="box">
            
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody>

                <tr>
                  <th>username</th>
                  <th>email</th>
                  <th>Action</th>
                </tr>
                
                <?php foreach ($userrequestlists as $userrequestlist) { 

                      $user=new User();

                      $userlist=$user->getUser($userrequestlist['userid']);
                ?>
                
                  <tr>
                    <td> <?php echo $userlist['firstname'];?> <?php echo $userlist['lastnaame'];?> </td>
                    <td> <?php echo $userlist['email'];?> </td>
                    <td> <a href="approve_registration.php?id=<?php echo $userrequestlist['userid'];?>">Approve</a> </td>
                    t
                  </tr>

                <?php } ?>

              </tbody></table>
            </div>
            <!-- /.box-body -->
        </div>
 <?php } ?>
    </section>
<?php include 'footer.php'; ?>

